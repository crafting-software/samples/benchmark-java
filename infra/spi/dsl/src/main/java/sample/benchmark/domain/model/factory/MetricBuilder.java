package sample.benchmark.domain.model.factory;

import com.google.common.collect.Lists;
import io.vavr.Tuple;
import io.vavr.Tuple2;
import sample.benchmark.domain.model.metric.Data;
import sample.benchmark.domain.model.metric.Metric;
import sample.benchmark.domain.model.metric.Unit;
import sample.benchmark.domain.model.value.Combination;
import sample.benchmark.domain.model.value.Combinations;
import sample.benchmark.domain.model.value.Value;

import java.util.List;

public final class MetricBuilder {
    private final String name;
    private Unit unit = Unit.none();
    private Combination combination = Combinations.sum();
    private final List<Tuple2<Reference, Value>> data = Lists.newArrayList();

    public static MetricBuilder metric(final String name) {
        return new MetricBuilder(name);
    }

    private MetricBuilder(final String name) {
        this.name = name;
    }

    public MetricBuilder unit(final Unit unit) {
        this.unit = unit;
        return this;
    }

    public MetricBuilder unit(final String value) {
        this.unit = Unit.unit(value);
        return this;
    }

    public MetricBuilder combination(final Combination combination) {
        this.combination = combination;
        return this;
    }

    public MetricBuilder data(final Reference reference, final Value value) {
        data.add(Tuple.of(reference, value));
        return this;
    }

    Metric build(final Reference path, final Symbols symbols) {
        final Reference current = path.add(name).hashtag("Metric");
        Metric metric = Metric.metric(name).combination(combination).unit(unit);
        symbols.register(current, metric.identifier());
        for (final Tuple2<Reference, Value> tuple : data) {
            metric = metric.data(Data.data(symbols.find(tuple._1), tuple._2));
        }
        return metric;
    }
}
