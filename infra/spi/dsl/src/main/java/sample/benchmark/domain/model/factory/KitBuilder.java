package sample.benchmark.domain.model.factory;

import io.vavr.collection.List;
import sample.benchmark.domain.model.product.FunctionalKit;

public final class KitBuilder {
    private final String name;
    private List<VariantBuilder> variants = List.empty();

    public static KitBuilder kit(final String name) {
        return new KitBuilder(name);
    }

    private KitBuilder(final String name) {
        this.name = name;
    }

    public KitBuilder with(final VariantBuilder builder) {
        variants = variants.append(builder);
        return this;
    }

    FunctionalKit build(final Reference path, final Symbols symbols) {
        final Reference current = path.add(name).hashtag("Kit");
        FunctionalKit kit = FunctionalKit.kit(name);
        symbols.register(current, kit.identifier());
        for (final VariantBuilder builder : variants) {
            kit = kit.with(builder.build(current, symbols));
        }
        return kit;
    }
}
