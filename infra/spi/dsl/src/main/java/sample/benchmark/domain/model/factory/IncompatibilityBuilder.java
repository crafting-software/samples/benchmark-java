package sample.benchmark.domain.model.factory;

import com.google.common.collect.Lists;
import sample.benchmark.domain.model.product.Incompatibility;

import java.util.List;

import static java.util.Arrays.asList;

public final class IncompatibilityBuilder {
    private final List<Reference> references = Lists.newArrayList();

    public static IncompatibilityBuilder incompatibilities() {
        return new IncompatibilityBuilder();
    }

    public IncompatibilityBuilder between(final Reference... references) {
        this.references.addAll(asList(references));
        return this;
    }

    Incompatibility build(final Symbols symbols) {
        return Incompatibility.incompatibility(io.vavr.collection.List.ofAll(references).map(symbols::find));
    }
}
