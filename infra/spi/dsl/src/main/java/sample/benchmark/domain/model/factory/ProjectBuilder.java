package sample.benchmark.domain.model.factory;

import com.google.common.collect.Lists;
import sample.benchmark.domain.model.product.ProductLine;
import sample.benchmark.domain.model.project.Project;

import java.util.List;

public final class ProjectBuilder {
    private final String name;
    private ProductLine product;
    private final List<MetricBuilder> metrics = Lists.newArrayList();
    private final List<CriteriaBuilder> criterion = Lists.newArrayList();


    public static ProjectBuilder project(final String name) {
        return new ProjectBuilder(name);
    }

    private ProjectBuilder(final String name) {
        this.name = name;
    }

    public ProjectBuilder on(final ProductLine product) {
        this.product = product;
        return this;
    }

    public ProjectBuilder with(final MetricBuilder builder) {
        metrics.add(builder);
        return this;
    }

    public ProjectBuilder with(final CriteriaBuilder builder) {
        criterion.add(builder);
        return this;
    }

    Project build(final Symbols symbols) {
        final Reference current = Reference.path(name).hashtag("Product");
        Project project = Project.project(name, product);
        symbols.register(current, project.identifier());

        for (final MetricBuilder builder : metrics) {
            project = project.add(builder.build(current, symbols));
        }

        for (final CriteriaBuilder builder : criterion) {
            project = project.add(builder.build(symbols));
        }

        return project;
    }
}
