package sample.benchmark.domain.model.factory;

import com.google.common.base.Joiner;
import io.vavr.collection.List;

import java.util.Objects;

public final class Reference {
    private final String hashtag;
    private final List<String> elements;

    public static Reference ref(final String input) {
        final String[] parts = input.split(":");
        return new Reference(parts[0].substring(1), List.of(parts[1].split("/")));
    }

    static Reference empty() {
        return new Reference("", List.empty());
    }

    static Reference path(final String... elements) {
        return new Reference("", List.of(elements));
    }

    Reference add(final String element) {
        return new Reference(hashtag, elements.append(element));
    }

    Reference hashtag(final String hashtag) {
        return new Reference(hashtag, elements);
    }

    private Reference(final String hashtag, final List<String> elements) {
        this.hashtag = hashtag;
        this.elements = elements;
    }

    private String render() {
        return "#" + hashtag + ":" + Joiner.on("/").join(elements);
    }

    @Override
    public String toString() {
        return render();
    }

    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Reference reference = (Reference) o;
        return Objects.equals(hashtag, reference.hashtag) &&
                Objects.equals(elements, reference.elements);
    }

    public int hashCode() {
        return Objects.hash(hashtag, elements);
    }
}
