package sample.benchmark.domain.model.factory;

import com.google.common.collect.Maps;
import sample.benchmark.domain.model.basics.Identifier;
import sample.benchmark.domain.model.product.ProductLine;
import sample.benchmark.domain.model.project.Project;

import java.util.Map;

public final class Symbols {
    private final Map<Reference, Identifier> symbols = Maps.newHashMap();

    Identifier find(final Reference reference) {
        if (!symbols.containsKey(reference)) {
            throw new RuntimeException("reference not found " + reference);
        }
        return symbols.get(reference);
    }

    void register(final Reference reference, final Identifier identifier) {
        symbols.put(reference, identifier);
    }

    public ProductLine build(final ProductBuilder builder) {
        return builder.build(this);
    }

    public Project build(final ProjectBuilder builder) {
        return builder.build(this);
    }
}
