package sample.benchmark.domain.model.factory;

import sample.benchmark.domain.model.project.Criteria;
import sample.benchmark.domain.model.project.CriteriaInstruction;
import sample.benchmark.domain.model.value.Value;

import java.util.function.Predicate;

import static sample.benchmark.domain.model.project.CriteriaInstruction.MINIMIZE;

public final class CriteriaBuilder {
    private Reference reference = Reference.empty();
    private Predicate<Value> predicate = value -> true;
    private CriteriaInstruction instruction = MINIMIZE;

    public static CriteriaBuilder criteria() {
        return new CriteriaBuilder();
    }

    private CriteriaBuilder() {
    }

    public CriteriaBuilder on(final Reference reference) {
        this.reference = reference;
        return this;
    }

    public CriteriaBuilder predicate(final Predicate<Value> predicate) {
        this.predicate = predicate;
        return this;
    }

    public CriteriaBuilder instruction(final CriteriaInstruction instruction) {
        this.instruction = instruction;
        return this;
    }

    Criteria build(final Symbols symbols) {
        return Criteria.criteria(symbols.find(reference)).predicate(predicate).instruction(instruction);
    }
}
