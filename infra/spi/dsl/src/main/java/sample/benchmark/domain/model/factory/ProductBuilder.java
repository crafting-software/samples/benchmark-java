package sample.benchmark.domain.model.factory;

import com.google.common.collect.Lists;
import sample.benchmark.domain.model.product.ProductLine;

import java.util.List;

public final class ProductBuilder {
    private final String name;
    private final List<KitBuilder> kits = Lists.newArrayList();
    private final List<IncompatibilityBuilder> incompatibilities = Lists.newArrayList();
    private final List<MetricBuilder> characteristics = Lists.newArrayList();


    public static ProductBuilder product(final String name) {
        return new ProductBuilder(name);
    }

    private ProductBuilder(final String name) {
        this.name = name;
    }

    public ProductBuilder with(final KitBuilder builder) {
        kits.add(builder);
        return this;
    }

    public ProductBuilder with(final IncompatibilityBuilder builder) {
        incompatibilities.add(builder);
        return this;
    }

    public ProductBuilder with(final MetricBuilder builder) {
        characteristics.add(builder);
        return this;
    }

    ProductLine build(final Symbols symbols) {
        final Reference current = Reference.path(name).hashtag("Product");
        ProductLine product = ProductLine.product(name);
        symbols.register(current, product.identifier());

        for (final KitBuilder builder : kits) {
            product = product.with(builder.build(current, symbols));
        }

        for (final IncompatibilityBuilder builder : incompatibilities) {
            product = product.with(builder.build(symbols));
        }

        for (final MetricBuilder builder : characteristics) {
            product = product.with(builder.build(current, symbols));
        }

        return product;
    }

    public ProductLine build() {
        return build(new Symbols());
    }

}
