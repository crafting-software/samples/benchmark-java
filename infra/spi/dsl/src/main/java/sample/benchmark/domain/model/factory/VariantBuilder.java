package sample.benchmark.domain.model.factory;

import sample.benchmark.domain.model.product.Variant;

public final class VariantBuilder {
    private final String name;

    public static VariantBuilder variant(final String name) {
        return new VariantBuilder(name);
    }

    private VariantBuilder(final String name) {
        this.name = name;
    }

    Variant build(final Reference path, final Symbols symbols) {
        final Reference current = path.add(name).hashtag("Variant");
        final Variant variant = Variant.variant(name);
        symbols.register(current, variant.identifier());
        return variant;
    }
}
