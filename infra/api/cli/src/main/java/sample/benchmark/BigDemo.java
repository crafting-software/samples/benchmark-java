package sample.benchmark;

import sample.benchmark.domain.model.factory.ProjectBuilder;
import sample.benchmark.domain.model.factory.Symbols;
import sample.benchmark.domain.model.product.ProductLine;
import sample.benchmark.domain.model.project.Project;
import sample.benchmark.domain.model.search.Search;
import sample.benchmark.domain.model.solution.Solution;
import sample.benchmark.domain.model.solution.Solutions;
import sample.benchmark.domain.model.value.Value;
import sample.benchmark.math.pareto.ParetoComparators;
import sample.benchmark.math.pareto.ParetoFront;

import java.util.Random;

import static sample.benchmark.domain.model.factory.CriteriaBuilder.criteria;
import static sample.benchmark.domain.model.factory.KitBuilder.kit;
import static sample.benchmark.domain.model.factory.MetricBuilder.metric;
import static sample.benchmark.domain.model.factory.ProductBuilder.product;
import static sample.benchmark.domain.model.factory.Reference.ref;
import static sample.benchmark.domain.model.factory.VariantBuilder.variant;
import static sample.benchmark.domain.model.search.Algorithms.bruteForce;
import static sample.benchmark.domain.model.value.Combinations.sum;
import static sample.benchmark.domain.model.value.ValuePredicates.between;
import static sample.benchmark.domain.model.value.ValuePredicates.lessThan;
import static sample.benchmark.domain.model.value.Values.quantity;

public final class BigDemo {

    private static final Random rnd = new Random(42);

    private static Value rnd(final int lower, final int upper) {
        return quantity((rnd.nextInt() % (upper - lower)) + lower);
    }

    private static final Symbols symbols = new Symbols();

    private static final ProductLine product = symbols.build(
            product("Big")

                    //GF 1
                    .with(kit("kit 1-1")
                            .with(variant("1"))
                            .with(variant("2"))
                            .with(variant("3"))
                            .with(variant("4")))

                    .with(kit("kit 1-2")
                            .with(variant("1"))
                            .with(variant("2"))
                            .with(variant("3"))
                            .with(variant("4")))

                    .with(kit("kit 1-3")
                            .with(variant("1"))
                            .with(variant("2"))
                            .with(variant("3"))
                            .with(variant("4")))

                    .with(kit("kit 1-4")
                            .with(variant("1"))
                            .with(variant("2"))
                            .with(variant("3"))
                            .with(variant("4")))

                    .with(kit("kit 1-5")
                            .with(variant("1"))
                            .with(variant("2"))
                            .with(variant("3"))
                            .with(variant("4")))

                    .with(kit("kit 1-6")
                            .with(variant("1"))
                            .with(variant("2"))
                            .with(variant("3"))
                            .with(variant("4")))

                    .with(kit("kit 1-7")
                            .with(variant("1"))
                            .with(variant("2"))
                            .with(variant("3"))
                            .with(variant("4")))

                    .with(kit("kit 1-8")
                            .with(variant("1"))
                            .with(variant("2"))
                            .with(variant("3"))
                            .with(variant("4")))

                    //GF 2
                    .with(kit("kit 2-1")
                            .with(variant("1"))
                            .with(variant("2"))
                            .with(variant("3"))
                            .with(variant("4")))

                    .with(kit("kit 2-2")
                            .with(variant("1"))
                            .with(variant("2"))
                            .with(variant("3"))
                            .with(variant("4")))

                    .with(metric("Weight")
                            .combination(sum())
                            .data(ref("#Variant:Big/kit 1-1/1"), rnd(250, 400))
                            .data(ref("#Variant:Big/kit 1-1/2"), rnd(250, 400))
                            .data(ref("#Variant:Big/kit 1-1/3"), rnd(250, 400))
                            .data(ref("#Variant:Big/kit 1-1/4"), rnd(250, 400))

                            .data(ref("#Variant:Big/kit 1-2/1"), rnd(250, 400))
                            .data(ref("#Variant:Big/kit 1-2/2"), rnd(250, 400))
                            .data(ref("#Variant:Big/kit 1-2/3"), rnd(250, 400))
                            .data(ref("#Variant:Big/kit 1-2/4"), rnd(250, 400))

                            .data(ref("#Variant:Big/kit 1-3/1"), rnd(250, 400))
                            .data(ref("#Variant:Big/kit 1-3/2"), rnd(250, 400))
                            .data(ref("#Variant:Big/kit 1-3/3"), rnd(250, 400))
                            .data(ref("#Variant:Big/kit 1-3/4"), rnd(250, 400))

                            .data(ref("#Variant:Big/kit 1-4/1"), rnd(250, 400))
                            .data(ref("#Variant:Big/kit 1-4/2"), rnd(250, 400))
                            .data(ref("#Variant:Big/kit 1-4/3"), rnd(250, 400))
                            .data(ref("#Variant:Big/kit 1-4/4"), rnd(250, 400))

                            .data(ref("#Variant:Big/kit 1-5/1"), rnd(250, 400))
                            .data(ref("#Variant:Big/kit 1-5/2"), rnd(250, 400))
                            .data(ref("#Variant:Big/kit 1-5/3"), rnd(250, 400))
                            .data(ref("#Variant:Big/kit 1-5/4"), rnd(250, 400))

                            .data(ref("#Variant:Big/kit 1-6/1"), rnd(250, 400))
                            .data(ref("#Variant:Big/kit 1-6/2"), rnd(250, 400))
                            .data(ref("#Variant:Big/kit 1-6/3"), rnd(250, 400))
                            .data(ref("#Variant:Big/kit 1-6/4"), rnd(250, 400))

                            .data(ref("#Variant:Big/kit 1-7/1"), rnd(250, 400))
                            .data(ref("#Variant:Big/kit 1-7/2"), rnd(250, 400))
                            .data(ref("#Variant:Big/kit 1-7/3"), rnd(250, 400))
                            .data(ref("#Variant:Big/kit 1-7/4"), rnd(250, 400))

                            .data(ref("#Variant:Big/kit 1-8/1"), rnd(250, 400))
                            .data(ref("#Variant:Big/kit 1-8/2"), rnd(250, 400))
                            .data(ref("#Variant:Big/kit 1-8/3"), rnd(250, 400))
                            .data(ref("#Variant:Big/kit 1-8/4"), rnd(250, 400))

                            .data(ref("#Variant:Big/kit 2-1/1"), rnd(250, 400))
                            .data(ref("#Variant:Big/kit 2-1/2"), rnd(250, 400))
                            .data(ref("#Variant:Big/kit 2-1/3"), rnd(250, 400))
                            .data(ref("#Variant:Big/kit 2-1/4"), rnd(250, 400))

                            .data(ref("#Variant:Big/kit 2-2/1"), rnd(250, 400))
                            .data(ref("#Variant:Big/kit 2-2/2"), rnd(250, 400))
                            .data(ref("#Variant:Big/kit 2-2/3"), rnd(250, 400))
                            .data(ref("#Variant:Big/kit 2-2/4"), rnd(250, 400)))


                    .with(metric("Price")
                            .combination(sum())
                            .data(ref("#Variant:Big/kit 1-1/1"), rnd(2500, 6000))
                            .data(ref("#Variant:Big/kit 1-1/2"), rnd(2500, 6000))
                            .data(ref("#Variant:Big/kit 1-1/3"), rnd(2500, 6000))
                            .data(ref("#Variant:Big/kit 1-1/4"), rnd(2500, 6000))

                            .data(ref("#Variant:Big/kit 1-2/1"), rnd(2500, 6000))
                            .data(ref("#Variant:Big/kit 1-2/2"), rnd(2500, 6000))
                            .data(ref("#Variant:Big/kit 1-2/3"), rnd(2500, 6000))
                            .data(ref("#Variant:Big/kit 1-2/4"), rnd(2500, 6000))

                            .data(ref("#Variant:Big/kit 1-3/1"), rnd(2500, 6000))
                            .data(ref("#Variant:Big/kit 1-3/2"), rnd(2500, 6000))
                            .data(ref("#Variant:Big/kit 1-3/3"), rnd(2500, 6000))
                            .data(ref("#Variant:Big/kit 1-3/4"), rnd(2500, 6000))

                            .data(ref("#Variant:Big/kit 1-4/1"), rnd(2500, 6000))
                            .data(ref("#Variant:Big/kit 1-4/2"), rnd(2500, 6000))
                            .data(ref("#Variant:Big/kit 1-4/3"), rnd(2500, 6000))
                            .data(ref("#Variant:Big/kit 1-4/4"), rnd(2500, 6000))

                            .data(ref("#Variant:Big/kit 1-5/1"), rnd(2500, 6000))
                            .data(ref("#Variant:Big/kit 1-5/2"), rnd(2500, 6000))
                            .data(ref("#Variant:Big/kit 1-5/3"), rnd(2500, 6000))
                            .data(ref("#Variant:Big/kit 1-5/4"), rnd(2500, 6000))

                            .data(ref("#Variant:Big/kit 1-6/1"), rnd(2500, 6000))
                            .data(ref("#Variant:Big/kit 1-6/2"), rnd(2500, 6000))
                            .data(ref("#Variant:Big/kit 1-6/3"), rnd(2500, 6000))
                            .data(ref("#Variant:Big/kit 1-6/4"), rnd(2500, 6000))

                            .data(ref("#Variant:Big/kit 1-7/1"), rnd(2500, 6000))
                            .data(ref("#Variant:Big/kit 1-7/2"), rnd(2500, 6000))
                            .data(ref("#Variant:Big/kit 1-7/3"), rnd(2500, 6000))
                            .data(ref("#Variant:Big/kit 1-7/4"), rnd(2500, 6000))

                            .data(ref("#Variant:Big/kit 1-8/1"), rnd(2500, 6000))
                            .data(ref("#Variant:Big/kit 1-8/2"), rnd(2500, 6000))
                            .data(ref("#Variant:Big/kit 1-8/3"), rnd(2500, 6000))
                            .data(ref("#Variant:Big/kit 1-8/4"), rnd(2500, 6000))

                            .data(ref("#Variant:Big/kit 2-1/1"), rnd(2500, 6000))
                            .data(ref("#Variant:Big/kit 2-1/2"), rnd(2500, 6000))
                            .data(ref("#Variant:Big/kit 2-1/3"), rnd(2500, 6000))
                            .data(ref("#Variant:Big/kit 2-1/4"), rnd(2500, 6000))

                            .data(ref("#Variant:Big/kit 2-2/1"), rnd(2500, 6000))
                            .data(ref("#Variant:Big/kit 2-2/2"), rnd(2500, 6000))
                            .data(ref("#Variant:Big/kit 2-2/3"), rnd(2500, 6000))
                            .data(ref("#Variant:Big/kit 2-2/4"), rnd(2500, 6000))));


    private static final Project project = symbols.build(ProjectBuilder.project("BigProduct")
            .on(product)

            .with(criteria().on(ref("#Metric:Big/Weight")).predicate(lessThan(4800)))

            .with(criteria().on(ref("#Metric:Big/Price")).predicate(between(40000, 50000))));


    public static void main(final String[] args) {
        final Search algorithm = bruteForce(project);
        ParetoFront<Solution> pareto = ParetoFront.empty(ParetoComparators.comparatorInDouble(Solutions.extractOf()));

        for (final Solution ignored : algorithm.iterate()) {
            if (ignored.isValid()) {
                pareto = pareto.add(ignored);
            }
            System.out.println(algorithm.status() + " # pareto front: " + pareto.size());
            /*for (final Solution solution : pareto.solutions()) {
                System.out.println(solution.describe());
            }
            System.out.println();*/
        }
        System.out.println("Rejected: " + algorithm.getRejectedCount());
        System.out.println("Solutions: " + algorithm.getSolutionCount());
    }
}
