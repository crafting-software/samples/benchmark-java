package sample.benchmark;

import sample.benchmark.domain.model.factory.ProjectBuilder;
import sample.benchmark.domain.model.factory.Symbols;
import sample.benchmark.domain.model.metric.Metric;
import sample.benchmark.domain.model.product.FunctionalKit;
import sample.benchmark.domain.model.product.ProductLine;
import sample.benchmark.domain.model.product.Variant;
import sample.benchmark.domain.model.project.Project;
import sample.benchmark.domain.model.search.Search;
import sample.benchmark.domain.model.solution.Solution;
import sample.benchmark.domain.model.solution.Solutions;
import sample.benchmark.math.pareto.ParetoComparators;
import sample.benchmark.math.pareto.ParetoFront;

import static sample.benchmark.domain.model.factory.CriteriaBuilder.criteria;
import static sample.benchmark.domain.model.factory.IncompatibilityBuilder.incompatibilities;
import static sample.benchmark.domain.model.factory.KitBuilder.kit;
import static sample.benchmark.domain.model.factory.MetricBuilder.metric;
import static sample.benchmark.domain.model.factory.ProductBuilder.product;
import static sample.benchmark.domain.model.factory.Reference.ref;
import static sample.benchmark.domain.model.factory.VariantBuilder.variant;
import static sample.benchmark.domain.model.metric.Unit.kg;
import static sample.benchmark.domain.model.project.CriteriaInstruction.MAXIMIZE;
import static sample.benchmark.domain.model.project.CriteriaInstruction.MINIMIZE;
import static sample.benchmark.domain.model.search.Algorithms.bruteForce;
import static sample.benchmark.domain.model.value.Combinations.mean;
import static sample.benchmark.domain.model.value.Combinations.sum;
import static sample.benchmark.domain.model.value.ValuePredicates.greaterOrEqual;
import static sample.benchmark.domain.model.value.ValuePredicates.lessThan;
import static sample.benchmark.domain.model.value.Values.grade;
import static sample.benchmark.domain.model.value.Values.quantity;

public final class CarDemo {

    private static final Symbols symbols = new Symbols();

    private static final ProductLine product = symbols.build(
            product("Car")

                    .with(kit("Motorisation")
                            .with(variant("Eco"))
                            .with(variant("Eco+"))
                            .with(variant("Racer")))

                    .with(kit("Design")
                            .with(variant("Basic"))
                            .with(variant("Omega"))
                            .with(variant("Alpha")))

                    .with(kit("Air Conditioner")
                            .with(variant("Without"))
                            .with(variant("Manual"))
                            .with(variant("Automatic")))

                    .with(kit("Equipment Pack")
                            .with(variant("None"))
                            .with(variant("Zen"))
                            .with(variant("City")))

                    .with(incompatibilities().
                            between(ref("#Variant:Car/Motorisation/Eco"), ref("#Variant:Car/Design/Alpha")))

                    .with(metric("Weight")
                            .unit(kg())
                            .combination(sum())
                            .data(ref("#Variant:Car/Motorisation/Eco"), quantity(300))
                            .data(ref("#Variant:Car/Motorisation/Eco+"), quantity(320))
                            .data(ref("#Variant:Car/Motorisation/Racer"), quantity(290))
                            .data(ref("#Variant:Car/Design/Basic"), quantity(800))
                            .data(ref("#Variant:Car/Design/Omega"), quantity(830))
                            .data(ref("#Variant:Car/Design/Alpha"), quantity(870))
                            .data(ref("#Variant:Car/Air Conditioner/Without"), quantity(0))
                            .data(ref("#Variant:Car/Air Conditioner/Manual"), quantity(3))
                            .data(ref("#Variant:Car/Air Conditioner/Automatic"), quantity(4))
                            .data(ref("#Variant:Car/Equipment Pack/None"), quantity(0))
                            .data(ref("#Variant:Car/Equipment Pack/Zen"), quantity(30))
                            .data(ref("#Variant:Car/Equipment Pack/City"), quantity(34)))

                    .with(metric("Price")
                            .unit("€")
                            .combination(sum())
                            .data(ref("#Variant:Car/Motorisation/Eco"), quantity(3000))
                            .data(ref("#Variant:Car/Motorisation/Eco+"), quantity(5000))
                            .data(ref("#Variant:Car/Motorisation/Racer"), quantity(6000))
                            .data(ref("#Variant:Car/Design/Basic"), quantity(12000))
                            .data(ref("#Variant:Car/Design/Omega"), quantity(15000))
                            .data(ref("#Variant:Car/Design/Alpha"), quantity(18000))
                            .data(ref("#Variant:Car/Air Conditioner/Without"), quantity(0))
                            .data(ref("#Variant:Car/Air Conditioner/Manual"), quantity(4000))
                            .data(ref("#Variant:Car/Air Conditioner/Automatic"), quantity(6000))
                            .data(ref("#Variant:Car/Equipment Pack/None"), quantity(0))
                            .data(ref("#Variant:Car/Equipment Pack/Zen"), quantity(6000))
                            .data(ref("#Variant:Car/Equipment Pack/City"), quantity(9000))));

    private static final Project project = symbols.build(ProjectBuilder.project("MyCar")
            .on(product)

            .with(metric("comfort")
                    .combination(mean())
                    .data(ref("#Variant:Car/Motorisation/Eco"), grade(0.6))
                    .data(ref("#Variant:Car/Motorisation/Eco+"), grade(0.8))
                    .data(ref("#Variant:Car/Motorisation/Racer"), grade(0.8))
                    .data(ref("#Variant:Car/Design/Basic"), grade(0.4))
                    .data(ref("#Variant:Car/Design/Omega"), grade(0.6))
                    .data(ref("#Variant:Car/Design/Alpha"), grade(0.8))
                    .data(ref("#Variant:Car/Air Conditioner/Without"), grade(0.2))
                    .data(ref("#Variant:Car/Air Conditioner/Manual"), grade(0.6))
                    .data(ref("#Variant:Car/Air Conditioner/Automatic"), grade(0.8))
                    .data(ref("#Variant:Car/Equipment Pack/None"), grade(0.2))
                    .data(ref("#Variant:Car/Equipment Pack/Zen"), grade(0.6))
                    .data(ref("#Variant:Car/Equipment Pack/City"), grade(0.6)))

            .with(metric("consumption")
                    .combination(mean())
                    .data(ref("#Variant:Car/Motorisation/Eco"), grade(0.7))
                    .data(ref("#Variant:Car/Motorisation/Eco+"), grade(0.8))
                    .data(ref("#Variant:Car/Motorisation/Racer"), grade(0.4))
                    .data(ref("#Variant:Car/Air Conditioner/Without"), grade(1.0))
                    .data(ref("#Variant:Car/Air Conditioner/Manual"), grade(0.4))
                    .data(ref("#Variant:Car/Air Conditioner/Automatic"), grade(0.3))
                    .data(ref("#Variant:Car/Equipment Pack/Zen"), grade(0.6))
                    .data(ref("#Variant:Car/Equipment Pack/City"), grade(0.5)))

            .with(criteria().on(ref("#Metric:Car/Weight")).instruction(MINIMIZE).predicate(lessThan(1160)))

            .with(criteria().on(ref("#Metric:Car/Price")).instruction(MINIMIZE))

            .with(criteria().on(ref("#Metric:MyCar/comfort")).instruction(MAXIMIZE).predicate(greaterOrEqual(0.6)))

            .with(criteria().on(ref("#Metric:MyCar/consumption")).instruction(MAXIMIZE).predicate(greaterOrEqual(0.5))));


    public static void main(final String[] args) {
        final Search algorithm = bruteForce(project);
        ParetoFront<Solution> pareto = ParetoFront.empty(ParetoComparators.comparatorInDouble(Solutions.extractOf()));
        for (final Solution solution : algorithm.iterate()) {
            System.out.println(algorithm.status() +  " # pareto front: " + pareto.size());

            if (solution.isValid()) {
                pareto = pareto.add(solution);
            }
        }
        System.out.println("Rejected: " + algorithm.getRejectedCount());
        System.out.println("Solutions: " + algorithm.getSolutionCount());
        System.out.println("Pareto optimums: " + pareto.size());

        for (final Solution solution : pareto.solutions()) {
            System.out.println(solution.describe());
        }

        normalizeMetrics();
    }

    private static void normalizeMetrics() {
        System.out.println("\n\n");
        for (final Metric metric : product.metrics().iterate()) {
            double min = 0;
            double max = 0;
            for (final FunctionalKit kit : product.kits().iterate()) {
                min += metric.min(kit.variants().iterate().map(Variant::identifier).toList()).value();
                max += metric.max(kit.variants().iterate().map(Variant::identifier).toList()).value();
            }
            System.out.println("" + metric.name().value() + " : min = " + min + "; max = " + max);
        }
    }
}
