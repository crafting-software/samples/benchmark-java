package sample.benchmark.math.constraints;

import com.google.common.base.Joiner;
import io.vavr.collection.List;
import sample.benchmark.math.Index;

import static sample.benchmark.math.constraints.Constraints.dimension;
import static sample.benchmark.math.constraints.Constraints.empty;

final class DimensionConstraint implements Constraint {
    private final List<Dimension> dimensions;

    DimensionConstraint(final List<Dimension> dimensions) {
        this.dimensions = dimensions;
    }

    public Constraint intersection(final Constraint another) {
        if (another instanceof EmptyConstraint)
            return empty();
        return dimension(dimensions.zipWith(((DimensionConstraint) another).dimensions, Dimension::intersection));
    }

    public int cardinality() {
        return dimensions.map(Dimension::cardinality).reduce((x, y) -> x * y);
    }

    public boolean isEmpty() {
        return false;
    }

    public String render() {
        return "{" + Joiner.on(", ").join(dimensions.map(Dimension::render)) + "}";
    }

    public boolean test(final Index index) {
        return dimensions.zipWithIndex((d, i) -> d.test(index.get(i))).forAll(v -> v);
    }
}
