package sample.benchmark.math.constraints;

import static sample.benchmark.math.constraints.Dimensions.zero;

final class OneElementDimension implements Dimension {
    private final int value;

    OneElementDimension(final int value) {
        this.value = value;
    }

    public Dimension intersection(final Dimension another) {
        if (another instanceof ManyElementsDimension) {
            return this;
        }
        if (another instanceof OneElementDimension && ((OneElementDimension) another).value != value) {
            return zero();
        }
        return another;
    }

    public int cardinality() {
        return 1;
    }

    public String render() {
        return "{" + value + "}";
    }

    public boolean test(final Integer value) {
        return value == this.value;
    }

}
