package sample.benchmark.math.constraints;

import io.vavr.collection.List;

public final class Constraints {
    public static Constraint empty() {
        return new EmptyConstraint();
    }

    public static Constraint dimension(final Dimension... values) {
        return dimension(List.of(values));
    }

    public static Constraint dimension(final List<Dimension> dimensions) {
        for (final Dimension dimension : dimensions) {
            if (dimension.cardinality() == 0) {
                return empty();
            }
        }
        return new DimensionConstraint(dimensions);
    }

    private enum Step {
        PLUS, MINUS;
    }

    private static class CardinalityAccululator {
        Step step = Step.PLUS;
        int cardinality = 0;

        public void accumulate(final int current) {
            switch (step) {
                case PLUS:
                    cardinality += current;
                    step = Step.MINUS;
                    break;
                case MINUS:
                    cardinality -= current;
                    step = Step.PLUS;
                    break;
            }
        }

        public int get() {
            return cardinality;
        }
    }

    public static int cardinalityOfUnion(List<Constraint> constraints) {
        final CardinalityAccululator accululator = new CardinalityAccululator();
        while (!constraints.isEmpty()) {
            accululator.accumulate(constraints.map(Constraint::cardinality).sum().intValue());
            constraints = unfold(constraints);
        }
        return accululator.get();
    }

    private static List<Constraint> unfold(List<Constraint> constraints) {
        List<Constraint> result = List.empty();
        while (!constraints.isEmpty()) {
            final Constraint first = constraints.head();
            constraints = constraints.tail();
            for (final Constraint constraint : constraints) {
                final Constraint intersection = first.intersection(constraint);
                if (!intersection.isEmpty())
                    result = result.append(intersection);
            }
        }
        return result;
    }
}
