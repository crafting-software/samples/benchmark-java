package sample.benchmark.math.constraints;

final class ManyElementsDimension implements Dimension {
    private final int cardinality;

    ManyElementsDimension(final int cardinality) {
        this.cardinality = cardinality;
    }

    public Dimension intersection(final Dimension another) {
        return another;
    }

    public int cardinality() {
        return cardinality;
    }

    public String render() {
        return "{0.." + cardinality + "}";
    }

    public boolean test(final Integer value) {
        return value < cardinality;
    }
}
