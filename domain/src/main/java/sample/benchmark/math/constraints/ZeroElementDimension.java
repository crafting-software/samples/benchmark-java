package sample.benchmark.math.constraints;

import static sample.benchmark.math.constraints.Dimensions.zero;

final class ZeroElementDimension implements Dimension {
    public Dimension intersection(final Dimension another) {
        return zero();
    }

    public int cardinality() {
        return 0;
    }

    public String render() {
        return "{}";
    }

    public boolean test(final Integer value) {
        return false;
    }
}
