package sample.benchmark.math.constraints;

import sample.benchmark.math.Index;

final class EmptyConstraint implements Constraint {
    public Constraint intersection(final Constraint another) {
        return this;
    }

    public int cardinality() {
        return 0;
    }

    public boolean isEmpty() {
        return true;
    }

    public String render() {
        return "{}";
    }

    public boolean test(final Index index) {
        return false;
    }
}
