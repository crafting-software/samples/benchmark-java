package sample.benchmark.math.constraints;

import sample.benchmark.math.Index;

import java.util.function.Predicate;

public interface Constraint extends Predicate<Index> {
    Constraint intersection(final Constraint another);

    int cardinality();

    boolean isEmpty();

    String render();
}
