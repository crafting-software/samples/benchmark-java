package sample.benchmark.math.constraints;

import java.util.function.Predicate;

public interface Dimension extends Predicate<Integer> {
    Dimension intersection(final Dimension another);

    int cardinality();

    String render();
}
