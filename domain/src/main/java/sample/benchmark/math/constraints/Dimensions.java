package sample.benchmark.math.constraints;

public final class Dimensions {
    public static Dimension zero() {
        return new ZeroElementDimension();
    }

    public static Dimension one(final int value) {
        return new OneElementDimension(value);
    }

    public static Dimension many(final int cardinality) {
        return new ManyElementsDimension(cardinality);
    }
}
