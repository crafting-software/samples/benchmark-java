package sample.benchmark.math;

import io.vavr.collection.Iterator;
import io.vavr.collection.List;
import io.vavr.control.Option;

import java.util.Random;
import java.util.function.Supplier;

import static java.lang.Math.abs;

public final class ConvexSpace implements Space {
    private final List<Integer> bases;

    public static ConvexSpace of(final int... bases) {
        return of(List.ofAll(bases));
    }

    public static ConvexSpace of(final List<Integer> bases) {
        return new ConvexSpace(bases);
    }

    private ConvexSpace(final List<Integer> bases) {
        this.bases = bases;
    }

    public long combinations() {
        return bases.map(Integer::longValue).reduce((left, right) -> left * right);
    }

    public int size() {
        return bases.size();
    }

    public Index first() {
        return generate(() -> 0);
    }

    public Option<Index> next(final Index current) {
        final int[] indexes = current.elements;
        final int[] next = new int[size()];
        boolean carry = true;
        for (int i = 0; i < size(); i++) {
            final int value = indexes[i] + (carry ? 1 : 0);
            carry = value >= bases.get(i);
            next[i] = carry ? 0 : value;
        }
        return carry ? Option.none() : Option.of(new Index(next));
    }

    public Index random() {
        final Random r = new Random();
        return generate(r::nextInt);
    }

    public Iterator<Index> iterate() {
        return IndexIterator.iterate(this);
    }

    public Index generate(final Supplier<Integer> source) {
        final int[] next = new int[size()];
        for (int i = 0; i < size(); i++) {
            next[i] = abs(source.get() % bases.get(i));
        }
        return new Index(next);
    }

    public boolean contains(final Index index) {
        if (index.size() != size()) {
            return false;
        }
        final int[] indexes = index.elements;
        for (int i = 0; i < size(); i++) {
            if (indexes[i] >= bases.get(i)) return false;
        }
        return true;
    }
}
