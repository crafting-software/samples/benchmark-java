package sample.benchmark.math;

import io.vavr.collection.Iterator;
import io.vavr.control.Option;

public interface Space {
    long combinations();

    int size();

    Index first();

    Option<Index> next(Index current);

    Index random();

    Iterator<Index> iterate();

    boolean contains(final Index index);
}
