package sample.benchmark.math.space.index;

import io.vavr.collection.Iterator;
import io.vavr.collection.List;
import io.vavr.control.Option;

public final class IndexIterator implements Iterator<Index> {
    final List<Integer> bases;
    private Index current;
    private boolean done = false;

    public static Iterator<Index> iterate(final int... bases) {
        return iterate(List.ofAll(bases));
    }

    public static Iterator<Index> iterate(final List<Integer> bases) {
        return new IndexIterator(bases);
    }

    private IndexIterator(final List<Integer> bases) {
        this.bases = bases;
        current = Index.zero(bases.length());
    }

    public boolean hasNext() {
        return !done;
    }

    public Index next() {
        final Index result = current;
        final Option<Index> next = inc(current);
        if (next.isDefined()) {
            current = next.get();
        } else {
            done = true;
        }
        return result;
    }

    public Option<Index> inc(final Index current) {
        final int[] indexes = current.elements;
        final int[] next = new int[current.size()];
        boolean carry = true;
        for (int i = 0; i < current.size(); i++) {
            final int value = indexes[i] + (carry ? 1 : 0);
            carry = value >= bases.get(i);
            next[i] = carry ? 0 : value;
        }
        return carry ? Option.none() : Option.of(new Index(next));
    }
}
