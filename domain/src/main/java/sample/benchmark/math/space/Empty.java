package sample.benchmark.math.space;

import io.vavr.collection.Iterator;

import java.util.Objects;

final class Empty implements Space {
    private final boolean value = true;

    public boolean isEmpty() {
        return true;
    }

    public long cardinality() {
        return 0;
    }

    public String render() {
        return "{}";
    }

    public Iterator<Coordinates> iterate() {
        return Iterator.empty();
    }

    public boolean contains(final Coordinates coordinates) {
        return false;
    }

    public String toString() {
        return render();
    }

    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Empty empty = (Empty) o;
        return value == empty.value;
    }

    public int hashCode() {
        return Objects.hash(value);
    }
}
