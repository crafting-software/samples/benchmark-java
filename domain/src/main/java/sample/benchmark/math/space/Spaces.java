package sample.benchmark.math.space;

import io.vavr.collection.List;
import io.vavr.collection.Map;
import io.vavr.collection.Set;
import sample.benchmark.math.space.dimension.Constraint;
import sample.benchmark.math.space.dimension.Constraints;
import sample.benchmark.math.space.dimension.Dimension;

import static sample.benchmark.math.space.Converter.convert;

public final class Spaces {
    public static Space empty() {
        return new Empty();
    }

    public static Space space(final Dimension... dimensions) {
        return space(List.of(dimensions));
    }

    public static Space space(final List<Dimension> dimensions) {
        if (dimensions.exists(Dimension::isEmpty)) {
            return empty();
        }
        return new ConvexSpace(dimensions);
    }

    public static Space union(final Space left, final Space right) {
        return new Union(left, right);
    }

    public static Space intersection(final Space first, final Space second) {
        if ((first.isEmpty()) || (second.isEmpty())) {
            return empty();
        }
        if (first instanceof ConvexSpace && second instanceof ConvexSpace) {
            final Map<String, Constraint> f = convert(((ConvexSpace) first).dimensions());
            final Map<String, Constraint> s = convert(((ConvexSpace) second).dimensions());
            final Set<String> intersect = f.keySet().intersect(s.keySet());
            if (intersect.length() != f.length()) {
                return empty();
            }
            if (intersect.length() != s.length()) {
                return empty();
            }
            return space(convert(
                    f
                            .filterKeys(intersect::contains)
                            .merge(s.filterKeys(intersect::contains), Constraints::intersection)));
        }

        if (first instanceof Union) {
            final Union f = (Union) first;
            return union(intersection(f.left(), second), intersection(f.right(), second));
        }

        if (second instanceof Union) {
            final Union s = (Union) second;
            return union(intersection(s.left(), first), intersection(s.right(), first));
        }

        throw new IllegalArgumentException();
    }

    public static Space difference(final Space left, final Space right) {
        return new Difference(left, right);
    }
}
