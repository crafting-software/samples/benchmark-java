package sample.benchmark.math.space;

import java.util.Objects;

public final class Coordinate {
    private final String name;
    private final int value;

    public static Coordinate c(final String name, final int value) {
        return new Coordinate(name, value);
    }

    private Coordinate(final String name, final int value) {
        this.name = name;
        this.value = value;
    }

    public String name() {
        return name;
    }

    public int value() {
        return value;
    }

    public String render() {
        return name + "=" + value;
    }

    public String toString() {
        return render();
    }

    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Coordinate that = (Coordinate) o;
        return value == that.value &&
                Objects.equals(name, that.name);
    }

    public int hashCode() {
        return Objects.hash(name, value);
    }
}
