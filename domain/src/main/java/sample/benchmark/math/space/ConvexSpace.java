package sample.benchmark.math.space;

import com.google.common.base.Joiner;
import io.vavr.collection.Iterator;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import sample.benchmark.math.space.dimension.Constraint;
import sample.benchmark.math.space.dimension.Constraints;
import sample.benchmark.math.space.dimension.Dimension;
import sample.benchmark.math.space.index.Index;
import sample.benchmark.math.space.index.IndexIterator;

import java.util.Objects;

import static sample.benchmark.math.space.Converter.convert;

final class ConvexSpace implements Space {
    private final List<Dimension> dimensions;

    ConvexSpace(final List<Dimension> dimensions) {
        this.dimensions = dimensions;
    }

    public boolean isEmpty() {
        return dimensions.isEmpty();
    }

    public long cardinality() {
        return dimensions.map(Dimension::cardinality).reduceLeftOption((x, y) -> x * y).getOrElse(0L);
    }

    public List<Dimension> dimensions() {
        return dimensions;
    }

    public String render() {
        return "{" + Joiner.on(", ").join(dimensions.map(Dimension::render)) + "}";
    }

    public Iterator<Coordinates> iterate() {
        final List<List<Coordinate>> values = dimensions.map(Dimension::values);
        final Iterator<Index> iterator = IndexIterator.iterate(dimensions.map(Dimension::cardinality).map(Long::intValue));

        return new Iterator<>() {
            public boolean hasNext() {
                return iterator.hasNext();
            }

            public Coordinates next() {
                return Coordinates.of(iterator.next().map((value, dimension) -> values.get(dimension).get(value)));
            }
        };
    }

    public boolean contains(final Coordinates coordinates) {
        final Map<String, Constraint> s = convert(dimensions);
        return coordinates.iterate().forAll(c -> s.get(c.name()).getOrElse(Constraints.none()).contains(c));
    }

    public String toString() {
        return render();
    }

    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final ConvexSpace that = (ConvexSpace) o;
        return Objects.equals(dimensions, that.dimensions);
    }

    public int hashCode() {
        return Objects.hash(dimensions);
    }
}
