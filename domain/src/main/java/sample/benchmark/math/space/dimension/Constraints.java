package sample.benchmark.math.space.dimension;

public final class Constraints {
    public static Constraint none() {
        return range(0, 0);
    }

    public static Constraint eq(final int value) {
        return range(value, value + 1);
    }

    public static Constraint range(final int to) {
        return range(0, to);
    }

    public static Constraint range(final int from, final int to) {
        if (from > to) {
            return none();
        }
        return new Constraint(from, to);
    }

    public static Constraint intersection(final Constraint first, final Constraint second) {
        return range(Math.max(first.from(), second.from()), Math.min(first.to(), second.to()));
    }
}
