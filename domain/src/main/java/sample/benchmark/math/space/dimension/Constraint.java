package sample.benchmark.math.space.dimension;

import io.vavr.collection.Iterator;
import io.vavr.collection.List;
import io.vavr.control.Option;
import sample.benchmark.math.space.Coordinate;

import java.util.Objects;

public final class Constraint {
    private final int from;
    private final int to;

    Constraint(final int from, final int to) {
        this.from = from;
        this.to = to;
    }

    public int from() {
        return from;
    }

    public int to() {
        return to;
    }

    public boolean isEmpty() {
        return from == to;
    }

    public long cardinality() {
        return to - from;
    }

    public List<Integer> values() {
        return List.range(from, to);
    }

    public boolean contains(final Coordinate coordinate) {
        return (from <= coordinate.value()) && (coordinate.value() < to);
    }

    public Option<Integer> first() {
        if (isEmpty()) {
            return Option.none();
        }
        return Option.of(from);
    }

    public Option<Integer> next(final int current) {
        if (current < from) return Option.none();
        if (current >= to - 1) return Option.none();
        return Option.of(current + 1);
    }

    public Iterator<Integer> iterate() {
        return Iterator.range(from, to);
    }

    public String render() {
        if (isEmpty()) {
            return "{}";
        }
        if (from == to - 1) {
            return "{" + from + "}";
        }
        return "{" + from + "..." + to + "}";
    }

    public String toString() {
        return render();
    }

    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Constraint constraint = (Constraint) o;
        return from == constraint.from &&
                to == constraint.to;
    }

    public int hashCode() {
        return Objects.hash(from, to);
    }
}
