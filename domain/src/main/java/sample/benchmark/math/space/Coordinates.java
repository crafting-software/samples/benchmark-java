package sample.benchmark.math.space;

import com.google.common.base.Joiner;
import io.vavr.collection.Iterator;
import io.vavr.collection.List;

import java.util.Objects;

public final class Coordinates {
    private final List<Coordinate> values;

    public static Coordinates of(final Coordinate... values) {
        return of(List.of(values));
    }

    public static Coordinates of(final List<Coordinate> values) {
        return new Coordinates(values);
    }

    private Coordinates(final List<Coordinate> values) {
        this.values = values;
    }

    public Iterator<Coordinate> iterate() {
        return values.iterator();
    }

    public String render() {
        return "(" + Joiner.on(", ").join(values.map(Coordinate::render)) + ")";
    }

    public String toString() {
        return render();
    }

    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Coordinates that = (Coordinates) o;
        return Objects.equals(values, that.values);
    }

    public int hashCode() {
        return Objects.hash(values);
    }
}
