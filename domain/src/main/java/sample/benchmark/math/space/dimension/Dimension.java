package sample.benchmark.math.space.dimension;

import io.vavr.collection.Iterator;
import io.vavr.collection.List;
import sample.benchmark.math.space.Coordinate;

import java.util.Objects;

public final class Dimension {
    private final String name;
    private final Constraint constraint;

    public static Dimension dimension(final String name, final Constraint constraint) {
        return new Dimension(name, constraint);
    }

    private Dimension(final String name, final Constraint constraint) {
        this.name = name;
        this.constraint = constraint;
    }

    public String name() {
        return name;
    }

    public Constraint constraint() {
        return constraint;
    }

    public boolean isEmpty() {
        return constraint.isEmpty();
    }

    public long cardinality() {
        return constraint.cardinality();
    }

    public List<Coordinate> values() {
        return constraint.values().map(v -> Coordinate.c(name, v));
    }

    public Iterator<Coordinate> iterate() {
        return constraint.iterate().map(v -> Coordinate.c(name, v));
    }

    public String render() {
        return name + ": " + constraint.render();
    }

    public String toString() {
        return render();
    }

    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Dimension dimension = (Dimension) o;
        return Objects.equals(name, dimension.name) &&
                Objects.equals(constraint, dimension.constraint);
    }

    public int hashCode() {
        return Objects.hash(name, constraint);
    }
}
