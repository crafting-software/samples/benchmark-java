package sample.benchmark.math.space;

import io.vavr.collection.Iterator;

import java.util.Objects;

import static sample.benchmark.math.space.Spaces.intersection;

final class Difference implements Space {
    private final Space left;
    private final Space right;

    Difference(final Space left, final Space right) {
        this.left = left;
        this.right = right;
    }

    public Space left() {
        return left;
    }

    public Space right() {
        return right;
    }

    public boolean isEmpty() {
        if (left.isEmpty()) {
            return true;
        }
        return cardinality() == 0;
    }

    public long cardinality() {
        return left.cardinality() - intersection(left, right).cardinality();
    }

    public String render() {
        return "D (" + left.render() + ", " + right.render() + ")";
    }

    public Iterator<Coordinates> iterate() {
        return left.iterate().filter(c -> !right.contains(c));
    }

    public boolean contains(final Coordinates coordinates) {
        return left.contains(coordinates) && !right.contains(coordinates);
    }

    public String toString() {
        return render();
    }

    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Difference union = (Difference) o;
        return Objects.equals(left, union.left) && Objects.equals(right, union.right);
    }

    public int hashCode() {
        return Objects.hash(left, right);
    }
}
