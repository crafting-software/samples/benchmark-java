package sample.benchmark.math.space;

import io.vavr.collection.List;
import io.vavr.collection.Map;
import io.vavr.collection.Traversable;
import sample.benchmark.math.space.dimension.Constraint;
import sample.benchmark.math.space.dimension.Dimension;

import static sample.benchmark.math.space.dimension.Dimension.dimension;

final class Converter {
    static Map<String, Constraint> convert(final List<Dimension> list) {
        return list.groupBy(Dimension::name)
                .mapValues(Traversable::head)
                .mapValues(Dimension::constraint);
    }

    static List<Dimension> convert(final Map<String, Constraint> map) {
        return map.map(t -> dimension(t._1, t._2)).toList();
    }
}
