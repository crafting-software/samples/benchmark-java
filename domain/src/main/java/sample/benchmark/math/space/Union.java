package sample.benchmark.math.space;

import io.vavr.collection.Iterator;

import java.util.Objects;

import static sample.benchmark.math.space.Spaces.intersection;

final class Union implements Space {
    private final Space left;
    private final Space right;

    Union(final Space left, final Space right) {
        this.left = left;
        this.right = right;
    }

    public Space left() {
        return left;
    }

    public Space right() {
        return right;
    }

    public boolean isEmpty() {
        return left.isEmpty() && right.isEmpty();
    }

    public long cardinality() {
        return left.cardinality() + right.cardinality() - intersection(left, right).cardinality();
    }

    public String render() {
        return "U (" + left.render() + ", " + right.render() + ")";
    }

    public Iterator<Coordinates> iterate() {
        return Iterator.concat(left.iterate(), right.iterate());
    }

    public boolean contains(final Coordinates coordinates) {
        return left.contains(coordinates) || right.contains(coordinates);
    }

    public String toString() {
        return render();
    }

    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Union union = (Union) o;
        return Objects.equals(left, union.left) && Objects.equals(right, union.right);
    }

    public int hashCode() {
        return Objects.hash(left, right);
    }
}
