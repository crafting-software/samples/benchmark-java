package sample.benchmark.math.space;

import io.vavr.collection.Iterator;

public interface Space {
    boolean isEmpty();

    long cardinality();

    String render();

    Iterator<Coordinates> iterate();

    boolean contains(final Coordinates coordinates);
}
