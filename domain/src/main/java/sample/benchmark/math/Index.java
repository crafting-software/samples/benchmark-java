package sample.benchmark.math;

import io.vavr.collection.List;

import java.util.Arrays;
import java.util.function.BiFunction;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.joining;

public final class Index {
    final int[] elements;

    public static Index index(final int... elements) {
        return new Index(elements);
    }

    Index(final int[] elements) {
        this.elements = elements;
    }

    public <T> List<T> map(final BiFunction<Integer, Integer, T> mapper) {
        return List.ofAll(elements).zipWithIndex(mapper);
    }

    public int size() {
        return elements.length;
    }

    public int get(final int index) {
        return elements[index];
    }

    public String toString() {
        return "(" + stream(elements).mapToObj(String::valueOf).collect(joining(", ")) + ")";
    }

    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Index index = (Index) o;
        return Arrays.equals(elements, index.elements);
    }

    public int hashCode() {
        return Arrays.hashCode(elements);
    }
}
