package sample.benchmark.math.pareto;

public interface ParetoComparator<T> {
    enum Result {
        EQUAL, FIRST_LT_SECOND, FIRST_GT_SECOND, ELSE
    }

    Result compare(final T first, final T second);
}
