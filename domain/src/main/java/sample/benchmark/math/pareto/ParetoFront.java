package sample.benchmark.math.pareto;

import io.vavr.collection.Iterator;
import io.vavr.collection.List;

/**
 * Maintains a Pareto front set
 * Keep all solutions which are not dominated.
 *
 * @param <T>
 */
public final class ParetoFront<T> {
    private final List<T> solutions;
    private final ParetoComparator<T> comparator;

    public static <T> ParetoFront<T> empty(final ParetoComparator<T> comparator) {
        return new ParetoFront<>(List.empty(), comparator);
    }

    public ParetoFront<T> add(final T solution) {
        if (solutions.size() == 0) {
            return new ParetoFront<>(List.of(solution), comparator);
        } else {
            final Iterator<T> iterator = solutions.iterator();
            boolean isDominated = false;
            boolean isContained = false;
            List<T> next = List.empty();
            while (((!isDominated) && (!isContained)) && (iterator.hasNext())) {
                final T existing = iterator.next();
                final ParetoComparator.Result result = comparator.compare(existing, solution);
                switch (result) {
                    case FIRST_LT_SECOND:
                        break;
                    case FIRST_GT_SECOND:
                        isDominated = true;
                        next = next.append(existing);
                        break;
                    case EQUAL:
                        next = next.append(existing);
                        isContained = true;
                        break;
                    case ELSE:
                        next = next.append(existing);
                        break;
                }
            }
            for (final T remaining : iterator) {
                next = next.append(remaining);
            }
            if (!isDominated && !isContained) {
                next = next.append(solution);
            }
            return new ParetoFront<>(next, comparator);
        }
    }

    private ParetoFront(final List<T> solutions, final ParetoComparator<T> comparator) {
        this.solutions = solutions;
        this.comparator = comparator;
    }

    public List<T> solutions() {
        return solutions;
    }

    public int size() {
        return solutions.size();
    }
}
