package sample.benchmark.math.pareto;

public final class ParetoComparators {

    public interface DoubleExtractor<T> {
        int size(final T t);

        double get(final T t, int i);
    }

    public static <T> ParetoComparator<T> comparatorInDouble(final DoubleExtractor<T> valueOf) {
        return (first, second) -> {
            boolean bestIsOne = false;
            boolean bestIsTwo = false;
            for (int i = 0; i < valueOf.size(first); i++) {
                final double value1 = valueOf.get(first, i);
                final double value2 = valueOf.get(second, i);
                if (value1 != value2) {
                    if (value1 < value2) {
                        bestIsOne = true;
                    }
                    if (value2 < value1) {
                        bestIsTwo = true;
                    }
                }
            }
            if (bestIsOne && bestIsTwo) {
                return ParetoComparator.Result.ELSE;
            }
            if (!bestIsOne && !bestIsTwo) {
                return ParetoComparator.Result.EQUAL;
            }
            if (bestIsOne) return ParetoComparator.Result.FIRST_GT_SECOND;
            return ParetoComparator.Result.FIRST_LT_SECOND;
        };
    }
}
