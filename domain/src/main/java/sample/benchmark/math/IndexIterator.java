package sample.benchmark.math;

import io.vavr.collection.Iterator;
import io.vavr.collection.List;
import io.vavr.control.Option;

public final class IndexIterator implements Iterator<Index> {
    private final Space space;
    private Index current;
    private boolean done = false;

    public static Iterator<Index> iterate(final List<Integer> bases) {
        return iterate(ConvexSpace.of(bases));
    }

    public static IndexIterator iterate(final int... bases) {
        return iterate(ConvexSpace.of(bases));
    }

    public static IndexIterator iterate(final Space space) {
        return new IndexIterator(space);
    }

    private IndexIterator(final Space space) {
        this.space = space;
        current = space.first();
    }

    public boolean hasNext() {
        return !done;
    }

    public Index next() {
        final Index result = current;
        final Option<Index> next = space.next(current);
        if (next.isDefined()) {
            current = next.get();
        } else {
            done = true;
        }
        return result;
    }
}
