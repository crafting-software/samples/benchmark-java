package sample.benchmark.math;

import io.vavr.collection.Iterator;
import io.vavr.collection.List;
import io.vavr.control.Option;
import sample.benchmark.math.constraints.Constraint;

public final class ConstrainedSpace implements Space {
    private final Space space;
    private final List<Constraint> constraints;

    public static ConstrainedSpace of(final ConvexSpace space) {
        return new ConstrainedSpace(space, List.empty());
    }

    public static ConstrainedSpace of(final int... bases) {
        return of(List.ofAll(bases));
    }

    public static ConstrainedSpace of(final List<Integer> bases) {
        return of(ConvexSpace.of(bases));
    }

    public ConstrainedSpace with(final Constraint constraint) {
        return new ConstrainedSpace(space, constraints.append(constraint));
    }

    private ConstrainedSpace(final Space space, final List<Constraint> constraints) {
        this.space = space;
        this.constraints = constraints;
    }

    public long combinations() {
        return space.combinations();
    }

    public int size() {
        return space.size();
    }

    public Index first() {
        return space.first();
    }

    public Option<Index> next(Index current) {
        while (true) {
            final Option<Index> next = space.next(current);
            if (next.isDefined()) {
                current = next.get();
                if (match(current)) {
                    return Option.some(current);
                }
            } else {
                return Option.none();
            }
        }
    }

    public Index random() {
        Index current = space.random();
        while (!match(current)) {
            current = space.random();
        }
        return current;
    }

    public Iterator<Index> iterate() {
        return IndexIterator.iterate(this);
    }

    public boolean contains(final Index index) {
        return space.contains(index) && match(index);
    }

    public boolean match(final Index index) {
        return constraints.forAll(c -> c.test(index));
    }
}
