
Name: string
Cardinality: int

Dimension: Name x Cardinality
Dimensions: list of Dimension

DimensionConstraint: AllElements | OneElement | ZeroElement

Space (Domain): Dimensions x Constraints
Constraints:  







S = space()
    .dimension("r", upTo(10))
    .dimension("s", upTo(6))
    .dimension("t", upTo(30))
    .dimension("u", upTo(7))
    .dimension("v", upTo(17))
    .excludeAll(space().dimension("r", eq(4)).dimension(u, eq(2)))
    
    .constraint(coordinate("r", 4), coordinate("u", 2))
    
    
s.cardinality()
s.iterate()
s.random()
s.contains(index)