package sample.benchmark.domain.usecase;

import io.vavr.control.Option;
import sample.benchmark.domain.model.basics.Identifier;
import sample.benchmark.domain.model.basics.Name;
import sample.benchmark.domain.model.product.ProductLine;
import sample.benchmark.domain.model.product.Variant;

public class AddVariant {
    private final ProductLineRepository repository;

    public AddVariant(final ProductLineRepository repository) {
        this.repository = repository;
    }

    public Option<ProductLine> process(final Identifier product, final Identifier kit, final Name name) {
        return repository.findById(product).map(p -> p.addVariant(kit, Variant.variant(name))).map(repository::save);
    }
}
