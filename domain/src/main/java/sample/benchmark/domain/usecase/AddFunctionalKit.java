package sample.benchmark.domain.usecase;

import io.vavr.control.Option;
import sample.benchmark.domain.model.basics.Identifier;
import sample.benchmark.domain.model.basics.Name;
import sample.benchmark.domain.model.product.ProductLine;

import static sample.benchmark.domain.model.product.FunctionalKit.kit;

public class AddFunctionalKit {
    private final ProductLineRepository repository;

    public AddFunctionalKit(final ProductLineRepository repository) {
        this.repository = repository;
    }

    public Option<ProductLine> process(final Identifier product, final Name name) {
        return repository.findById(product).map(p -> p.with(kit(name))).map(repository::save);
    }
}
