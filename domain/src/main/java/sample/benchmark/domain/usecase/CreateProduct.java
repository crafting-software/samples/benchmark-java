package sample.benchmark.domain.usecase;

import sample.benchmark.domain.model.basics.Name;
import sample.benchmark.domain.model.product.ProductLine;

import static sample.benchmark.domain.model.product.ProductLine.product;

public class CreateProduct {
    private final ProductLineRepository repository;

    public CreateProduct(final ProductLineRepository repository) {
        this.repository = repository;
    }

    public ProductLine process(final Name name) {
        return repository.save(product(name));
    }
}
