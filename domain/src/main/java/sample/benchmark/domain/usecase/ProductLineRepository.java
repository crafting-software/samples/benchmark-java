package sample.benchmark.domain.usecase;

import io.vavr.control.Option;
import sample.benchmark.domain.model.basics.Identifier;
import sample.benchmark.domain.model.product.ProductLine;

public interface ProductLineRepository {
    Option<ProductLine> findById(Identifier product);

    ProductLine save(ProductLine product);
}
