# Learning and future improvements

## Define and entity ?

Lot of objects have identity/name/description ...


## Experiment with repository and usecases




## Clarify the concept of Value

It's not clear that we need different kind of value.


## Try to rank results

Normalize differences into 0..1 and sum the square of delta.


## Correct incompatibilities by encapsulating collections 

We flatten the set of incompatibilities which is a bad behaviour.
We should keep each incompatibilities set separate and wrap the 
management of incompatibilities.


## Rework Reference and Path

Theses two concepts are very similar.
