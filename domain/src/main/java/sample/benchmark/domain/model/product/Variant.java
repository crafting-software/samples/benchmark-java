package sample.benchmark.domain.model.product;

import sample.benchmark.domain.model.basics.Component;
import sample.benchmark.domain.model.basics.Identifier;
import sample.benchmark.domain.model.basics.Name;
import sample.benchmark.domain.model.basics.Path;

public final class Variant {
    private final Component component;

    public static Variant variant(final String name) {
        return variant(Name.of(name));
    }

    public static Variant variant(final Name name) {
        return new Variant(Component.component(name));
    }

    public Variant path(final Path path) {
        return new Variant(component.path(path));
    }

    public Variant path(final Name... elements) {
        return path(Path.path(elements));
    }

    public Variant name(final Name name) {
        return new Variant(component.name(name));
    }

    private Variant(final Component component) {
        this.component = component;
    }

    public Identifier identifier() {
        return component.identifier();
    }

    public Path path() {
        return component.path();
    }

    public Name name() {
        return component.name();
    }
}