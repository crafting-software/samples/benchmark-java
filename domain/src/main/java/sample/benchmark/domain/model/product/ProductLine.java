package sample.benchmark.domain.model.product;

import io.vavr.collection.Set;
import io.vavr.control.Option;
import sample.benchmark.domain.model.basics.Component;
import sample.benchmark.domain.model.basics.Identifier;
import sample.benchmark.domain.model.basics.Name;
import sample.benchmark.domain.model.metric.Metric;
import sample.benchmark.domain.model.metric.Metrics;
import sample.benchmark.domain.model.solution.Solution;
import sample.benchmark.math.Index;
import sample.benchmark.math.Space;

public final class ProductLine {
    private final Component component;
    private final FunctionalKits kits;
    private final Metrics metrics;
    private final Incompatibilities incompatibilities;

    public static ProductLine product(final String name) {
        return product(Name.of(name));
    }

    public static ProductLine product(final Name name) {
        return new ProductLine(Component.component(name), FunctionalKits.empty(), Metrics.empty(), Incompatibilities.empty());
    }

    public ProductLine name(final Name name) {
        return new ProductLine(component.name(name), kits, metrics, incompatibilities);
    }

    public ProductLine with(final FunctionalKit kit) {
        return new ProductLine(component, kits.add(kit.path(kit.name())), metrics, incompatibilities);
    }

    public ProductLine addVariant(final Identifier kit, final Variant variant) {
        return kit(kit).map(k -> k.with(variant)).map(this::with).getOrElse(this);
    }

    public ProductLine with(final Metric metric) {
        return new ProductLine(component, kits, metrics.add(metric), incompatibilities);
    }

    public ProductLine with(final Incompatibility incompatibility) {
        return new ProductLine(component, kits, metrics, incompatibilities.add(incompatibility));
    }

    private ProductLine(final Component component, final FunctionalKits kits, final Metrics metrics, final Incompatibilities incompatibilities) {
        this.component = component;
        this.kits = kits;
        this.metrics = metrics;
        this.incompatibilities = incompatibilities;
    }

    public Identifier identifier() {
        return component.identifier();
    }

    public Name name() {
        return component.name();
    }

    public FunctionalKits kits() {
        return kits;
    }

    public Metrics metrics() {
        return metrics;
    }

    public Option<FunctionalKit> kit(final Identifier identifier) {
        return kits.kit(identifier);
    }

    public Set<Identifier> incompatibilitiesFor(final Identifier variant) {
        return incompatibilities.incompatibilitiesFor(variant);
    }

    public Space solutionSpace() {
        return kits.solutionSpace();
    }

    public Solution evaluateFor(final Index index) {
        return addMetrics(addIncompatibility(kits.evaluateFor(index)));
    }

    private Solution addIncompatibility(final Solution next) {
        return next.addIncompatibilities(next.composition().map(Variant::identifier).flatMap(this::incompatibilitiesFor).toSet());
    }

    private Solution addMetrics(Solution solution) {
        for (final Metric metric : metrics.content()) {
            solution = solution.addMetric(metric);
        }
        return solution;
    }

    public String toString() {
        return "ProductLine{" +
                "identifier=" + component.identifier() +
                ", name=" + component.name() +
                ", description=" + component.description() +
                ", content=" + kits +
                ", metrics=" + metrics +
                ", incompatibilities=" + incompatibilities +
                '}';
    }
}
