package sample.benchmark.domain.model.solution;

import sample.benchmark.domain.model.metric.Metric;
import sample.benchmark.domain.model.project.Criteria;
import sample.benchmark.domain.model.value.Value;

public final class SolutionViolateCriteria implements SolutionRational {
    private final Criteria criteria;
    private final Metric metric;
    private final Value value;

    SolutionViolateCriteria(final Criteria criteria, final Metric metric, final Value value) {
        this.criteria = criteria;
        this.metric = metric;
        this.value = value;
    }

    public String describe() {
        return "Violate criteria on " + metric.name().value();
    }
}
