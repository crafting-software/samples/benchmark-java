package sample.benchmark.domain.model.value;

import static sample.benchmark.domain.model.value.Values.quantity;

public final class Combinations {
    public static Combination sum() {
        return values -> quantity(values.map(Value::value).sum().doubleValue());
    }

    public static Combination mean() {
        return values -> quantity(values.map(Value::value).sum().doubleValue() / values.size());
    }
}
