package sample.benchmark.domain.model.basics;

import com.google.common.base.Joiner;
import io.vavr.collection.List;

import java.util.Objects;

public final class Path {
    private final String hashtag;
    private final List<String> elements;

    public static Path from(final String input) {
        final String[] parts = input.split(":");
        return new Path(parts[0].substring(1), List.of(parts[1].split("/")));
    }

    public static Path empty() {
        return new Path("", List.empty());
    }

    public static Path path(final String... elements) {
        return new Path("", List.of(elements));
    }

    public static Path path(final Name... elements) {
        return new Path("", List.of(elements).map(Name::value));
    }

    Path add(final String element) {
        return new Path(hashtag, elements.append(element));
    }

    Path hashtag(final String hashtag) {
        return new Path(hashtag, elements);
    }

    private Path(final String hashtag, final List<String> elements) {
        this.hashtag = hashtag;
        this.elements = elements;
    }

    public String render() {
        return "#" + hashtag + ":" + Joiner.on("/").join(elements);
    }

    @Override
    public String toString() {
        return render();
    }

    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Path reference = (Path) o;
        return Objects.equals(hashtag, reference.hashtag) &&
                Objects.equals(elements, reference.elements);
    }

    public int hashCode() {
        return Objects.hash(hashtag, elements);
    }
}
