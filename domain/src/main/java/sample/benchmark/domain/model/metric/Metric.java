package sample.benchmark.domain.model.metric;

import io.vavr.collection.List;
import sample.benchmark.domain.model.basics.Component;
import sample.benchmark.domain.model.basics.Identifier;
import sample.benchmark.domain.model.basics.Name;
import sample.benchmark.domain.model.value.Combination;
import sample.benchmark.domain.model.value.Combinations;
import sample.benchmark.domain.model.value.Value;

import java.util.Objects;

import static sample.benchmark.domain.model.value.Values.quantity;

public final class Metric {
    private final Component component;
    private final Unit unit;
    private final Combination combination;
    private final List<Data> data;

    public static Metric metric(final String name) {
        return metric(Name.of(name));
    }

    public static Metric metric(final Name name) {
        return new Metric(Component.component(name), Unit.none(), Combinations.sum(), List.empty());
    }

    public Metric name(final Name name) {
        return new Metric(component.name(name), unit, combination, data);
    }

    public Metric unit(final Unit unit) {
        return new Metric(component, unit, combination, data);
    }

    public Metric combination(final Combination combination) {
        return new Metric(component, unit, combination, data);
    }

    public Metric data(final Data data) {
        return new Metric(component, unit, combination, this.data.append(data));
    }

    private Metric(final Component component, final Unit unit, final Combination combination, final List<Data> data) {
        this.component = component;
        this.unit = unit;
        this.combination = combination;
        this.data = data;
    }

    public Identifier identifier() {
        return component.identifier();
    }

    public Name name() {
        return component.name();
    }

    public Unit unit() {
        return unit;
    }

    public Value combineVariants(final List<Identifier> variants) {
        return combination.apply(variants.flatMap(v -> data.filter(d -> d.isRelatedTo(v))).map(Data::value));
    }

    public Value min(final List<Identifier> variants) {
        return quantity(variants.flatMap(v -> data.filter(d -> d.isRelatedTo(v))).map(Data::value).map(Value::value).min().getOrElse(0.0));
    }

    public Value max(final List<Identifier> variants) {
        return quantity(variants.flatMap(v -> data.filter(d -> d.isRelatedTo(v))).map(Data::value).map(Value::value).max().getOrElse(0.0));
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Metric that = (Metric) o;
        return Objects.equals(component, that.component);
    }

    @Override
    public int hashCode() {
        return Objects.hash(component);
    }
}
