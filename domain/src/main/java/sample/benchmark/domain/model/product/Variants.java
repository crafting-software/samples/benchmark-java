package sample.benchmark.domain.model.product;

import io.vavr.collection.Iterator;
import io.vavr.collection.LinkedHashMap;
import io.vavr.collection.Map;
import sample.benchmark.domain.model.basics.Identifier;

public final class Variants {
    private final Map<Identifier, Variant> variants;

    public static Variants empty() {
        return new Variants(LinkedHashMap.empty());
    }

    public Variants add(final Variant variant) {
        return new Variants(variants.put(variant.identifier(), variant));
    }

    private Variants(final Map<Identifier, Variant> variants) {
        this.variants = variants;
    }

    public int count() {
        return variants.length();
    }

    public Variant findByIndex(final int index) {
        return variants.values().get(index);
    }

    public Iterator<Variant> iterate() {
        return variants.values().iterator();
    }
}
