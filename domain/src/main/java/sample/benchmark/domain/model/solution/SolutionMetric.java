package sample.benchmark.domain.model.solution;

import io.vavr.Tuple2;
import io.vavr.collection.HashMap;
import io.vavr.collection.Map;
import io.vavr.control.Option;
import sample.benchmark.domain.model.metric.Metric;
import sample.benchmark.domain.model.project.Criteria;
import sample.benchmark.domain.model.value.Value;

import java.util.Objects;

public final class SolutionMetric {
    private final Map<Metric, Value> values;

    public static SolutionMetric empty() {
        return new SolutionMetric(HashMap.empty());
    }

    public SolutionMetric add(final Metric metric, final Value value) {
        return new SolutionMetric(values.put(metric, value));
    }

    private SolutionMetric(final Map<Metric, Value> values) {
        this.values = values;
    }

    public Map<Metric, Value> values() {
        return values;
    }

    public Option<Tuple2<Metric, Value>> findMetric(final Criteria criteria) {
        return values.find(t -> Objects.equals(criteria.metric(), t._1.identifier()));
    }

    public String describe() {
        final StringBuilder builder = new StringBuilder();
        for (final Tuple2<Metric, Value> tuple : values) {
            builder.append(tuple._1.name().value()).append(": ").append(tuple._2.value()).append(" ").append(tuple._1.unit().value()).append("\n");
        }
        return builder.toString();
    }
}
