package sample.benchmark.domain.model.metric;

import sample.benchmark.domain.model.basics.Identifier;
import sample.benchmark.domain.model.value.Value;

public final class Data {
    private final Identifier variant;
    private final Value value;

    public static Data data(final Identifier variant, final Value value) {
        return new Data(variant, value);
    }

    private Data(final Identifier variant, final Value value) {
        this.variant = variant;
        this.value = value;
    }

    public Identifier variant() {
        return variant;
    }

    public Value value() {
        return value;
    }

    public boolean isRelatedTo(final Identifier variant) {
        return this.variant.equals(variant);
    }

    public String toString() {
        return "CharacteristicData{" +
                ", variant=" + variant +
                ", value=" + value +
                '}';
    }
}
