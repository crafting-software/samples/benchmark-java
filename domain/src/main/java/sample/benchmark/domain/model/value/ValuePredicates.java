package sample.benchmark.domain.model.value;

import io.vavr.Predicates;

import java.util.function.Predicate;

public final class ValuePredicates {
    public static Predicate<Value> greaterThan(final double literal) {
        return value -> value.value() > literal;
    }

    public static Predicate<Value> greaterOrEqual(final double literal) {
        return value -> value.value() >= literal;
    }

    public static Predicate<Value> lessThan(final double literal) {
        return value -> value.value() < literal;
    }

    public static Predicate<Value> lessOrEqual(final double literal) {
        return value -> value.value() <= literal;
    }

    public static Predicate<Value> between(final double lower, final double upper) {
        return Predicates.allOf(greaterOrEqual(lower), lessOrEqual(upper));
    }
}
