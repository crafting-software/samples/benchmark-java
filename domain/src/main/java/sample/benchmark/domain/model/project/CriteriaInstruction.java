package sample.benchmark.domain.model.project;

public enum CriteriaInstruction {
    MAXIMIZE,
    MINIMIZE
}
