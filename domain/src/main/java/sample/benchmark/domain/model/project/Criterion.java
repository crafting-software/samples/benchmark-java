package sample.benchmark.domain.model.project;

import io.vavr.collection.List;

public final class Criterion {
    private final List<Criteria> criterion;

    public static Criterion empty() {
        return new Criterion(List.empty());
    }

    public Criterion add(final Criteria criteria) {
        return new Criterion(criterion.append(criteria));
    }

    private Criterion(final List<Criteria> criterion) {
        this.criterion = criterion;
    }

    public List<Criteria> content() {
        return criterion;
    }
}
