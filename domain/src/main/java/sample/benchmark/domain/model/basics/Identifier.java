package sample.benchmark.domain.model.basics;

import java.util.Objects;
import java.util.UUID;
import java.util.function.Predicate;

import static java.util.regex.Pattern.CASE_INSENSITIVE;
import static java.util.regex.Pattern.compile;

public final class Identifier {
    private final static Predicate<String> PATTERN = compile("^[0-9a-z\\-]*$", CASE_INSENSITIVE).asPredicate();

    private final String value;

    public static Identifier empty() {
        return id("");
    }

    public static Identifier uuid() {
        return id(UUID.randomUUID().toString());
    }

    public static Identifier id(final String value) {
        if (PATTERN.test(value)) {
            return new Identifier(value);
        }
        throw new RuntimeException("invalid identifier: " + value);
    }

    private Identifier(final String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return "Identifier{value='" + value + "'}";
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Identifier that = (Identifier) o;
        return Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
