package sample.benchmark.domain.model.search;

import sample.benchmark.domain.model.solution.Solution;

import java.util.concurrent.TimeUnit;

public final class SearchStatistics {
    private static final long CHUNK_SIZE = 1000;

    private final long total;
    private final long startTime;
    private long rejectedCount = 0;
    private long solutionCount = 0;
    private long current = 0;
    private long chunkCount = 0;
    private long speed = 0;
    private long estimated = 0;
    private long chunk = CHUNK_SIZE;

    public SearchStatistics(final long total) {
        this.total = total;
        startTime = System.currentTimeMillis();
    }

    public void update(final Solution solution) {
        current++;
        chunk--;
        if (chunk == 0) {
            final long endTime = System.currentTimeMillis();
            chunkCount++;
            chunk = CHUNK_SIZE;
            speed = (endTime - startTime) / chunkCount;
            estimated = speed * total / CHUNK_SIZE;
        }

        if (!solution.isValid()) {
            rejectedCount++;
        } else {
            solutionCount++;
        }
    }

    public long getTotal() {
        return total;
    }

    public long getRejectedCount() {
        return rejectedCount;
    }

    public long getSolutionCount() {
        return solutionCount;
    }

    public String status() {
        return "" + current + " / " + total + " | rejected=" + rejectedCount + " | found=" + solutionCount + " | speed=" + speed + "| estimated=" + TimeUnit.MILLISECONDS.toMinutes(estimated);
    }

}
