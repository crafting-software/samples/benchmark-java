package sample.benchmark.domain.model.metric;

public final class Unit {
    private final String value;


    public static Unit none() {
        return unit("");
    }

    public static Unit kg() {
        return unit("kg");
    }

    public static Unit unit(final String value) {
        return new Unit(value);
    }

    private Unit(final String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }
}
