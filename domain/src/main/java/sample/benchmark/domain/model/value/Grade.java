package sample.benchmark.domain.model.value;

import java.util.Objects;

public final class Grade implements Value {
    private final double value;

    Grade(final double value) {
        this.value = value;
    }

    public double value() {
        return value;
    }

    @Override
    public String toString() {
        return "Grade{" +
                "value=" + value +
                '}';
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Grade grade = (Grade) o;
        return Double.compare(grade.value, value) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
