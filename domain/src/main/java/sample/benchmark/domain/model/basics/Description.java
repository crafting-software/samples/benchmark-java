package sample.benchmark.domain.model.basics;

import java.util.Objects;

public final class Description {
    private final String value;

    public static Description empty() {
        return new Description("");
    }

    public static Description of(final String value) {
        return new Description(value);
    }

    private Description(final String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return "Description{value='" + value + "'}";
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Description that = (Description) o;
        return Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
