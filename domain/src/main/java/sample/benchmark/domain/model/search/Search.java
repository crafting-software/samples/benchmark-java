package sample.benchmark.domain.model.search;

import io.vavr.collection.Iterator;
import sample.benchmark.domain.model.solution.Solution;

public interface Search {
    Iterator<Solution> iterate();

    long getRejectedCount();

    long getSolutionCount();

    String status();
}
