package sample.benchmark.domain.model.search;

import io.vavr.collection.Iterator;
import sample.benchmark.domain.model.project.Project;
import sample.benchmark.domain.model.solution.Solution;
import sample.benchmark.math.Space;

public final class Random implements Search {
    private final Project project;
    private SearchStatistics statistics;

    Random(final Project project) {
        this.project = project;
    }

    public Iterator<Solution> iterate() {
        statistics = new SearchStatistics(project.solutionSpace().combinations());
        final Space space = project.solutionSpace();

        return new Iterator<>() {
            public boolean hasNext() {
                return true;
            }

            public Solution next() {
                final Solution solution = project.evaluateFor(space.random());
                statistics.update(solution);
                return solution;
            }
        };
    }

    public long getRejectedCount() {
        return statistics.getRejectedCount();
    }

    public long getSolutionCount() {
        return statistics.getSolutionCount();
    }

    public String status() {
        return statistics.status();
    }
}