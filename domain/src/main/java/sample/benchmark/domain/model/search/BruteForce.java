package sample.benchmark.domain.model.search;

import io.vavr.collection.Iterator;
import sample.benchmark.domain.model.project.Project;
import sample.benchmark.domain.model.solution.Solution;
import sample.benchmark.math.Index;

public final class BruteForce implements Search {
    private final Project project;
    private SearchStatistics statistics;

    BruteForce(final Project project) {
        this.project = project;
    }

    public Iterator<Solution> iterate() {
        statistics = new SearchStatistics(project.solutionSpace().combinations());
        final Iterator<Index> iterator = project.solutionSpace().iterate();

        return new Iterator<>() {
            public boolean hasNext() {
                return iterator.hasNext();
            }

            public Solution next() {
                final Solution solution = project.evaluateFor(iterator.next());
                statistics.update(solution);
                return solution;
            }
        };
    }

    public long getRejectedCount() {
        return statistics.getRejectedCount();
    }

    public long getSolutionCount() {
        return statistics.getSolutionCount();
    }

    public String status() {
        return statistics.status();
    }
}