package sample.benchmark.domain.model.project;

import sample.benchmark.domain.model.basics.Identifier;
import sample.benchmark.domain.model.value.Value;

import java.util.function.Predicate;

import static sample.benchmark.domain.model.project.CriteriaInstruction.MINIMIZE;

public final class Criteria {
    private final Identifier identifier;
    private final Predicate<Value> predicate;
    private final CriteriaInstruction instruction;

    public static Criteria criteria(final Identifier identifier) {
        return new Criteria(identifier, value -> true, MINIMIZE);
    }

    public Criteria predicate(final Predicate<Value> predicate) {
        return new Criteria(identifier, predicate, instruction);
    }

    public Criteria instruction(final CriteriaInstruction instruction) {
        return new Criteria(identifier, predicate, instruction);
    }

    private Criteria(final Identifier identifier, final Predicate<Value> predicate, final CriteriaInstruction instruction) {
        this.identifier = identifier;
        this.predicate = predicate;
        this.instruction = instruction;
    }

    public Identifier metric() {
        return identifier;
    }

    public Predicate<Value> predicate() {
        return predicate;
    }

    public CriteriaInstruction instruction() {
        return instruction;
    }
}
