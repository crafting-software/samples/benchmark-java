package sample.benchmark.domain.model.product;

import io.vavr.collection.List;
import io.vavr.collection.Set;
import sample.benchmark.domain.model.basics.Identifier;

public final class Incompatibility {
    private final Set<Identifier> variants;

    public static Incompatibility incompatibility(final List<Identifier> identifiers) {
        return new Incompatibility(identifiers.toSet());
    }

    private Incompatibility(final Set<Identifier> variants) {
        this.variants = variants;
    }

    public Set<Identifier> variants() {
        return variants;
    }

    public boolean isRelatedTo(final Identifier variant) {
        return variants.contains(variant);
    }

    public String toString() {
        return "Incompatibility{" +
                "variants=" + variants +
                '}';
    }
}
