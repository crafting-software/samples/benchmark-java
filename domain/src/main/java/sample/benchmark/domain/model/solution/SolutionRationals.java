package sample.benchmark.domain.model.solution;

import io.vavr.collection.List;
import sample.benchmark.domain.model.metric.Metric;
import sample.benchmark.domain.model.product.Variant;
import sample.benchmark.domain.model.project.Criteria;
import sample.benchmark.domain.model.value.Value;

final class SolutionRationals {
    static SolutionRational criteriaViolation(final Criteria criteria, final Metric metric, final Value value) {
        return new SolutionViolateCriteria(criteria, metric, value);
    }

    static SolutionRational variantsIncompatible(final List<Variant> variants) {
        return new SolutionIncompatible(variants);
    }
}
