package sample.benchmark.domain.model.value;

public interface Value {
    double value();
}
