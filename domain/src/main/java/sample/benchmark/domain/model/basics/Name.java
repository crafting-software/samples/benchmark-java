package sample.benchmark.domain.model.basics;

import java.util.Objects;

public final class Name {
    private final String value;

    public static Name of(final String value) {
        return new Name(value);
    }

    private Name(final String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return "Name{value='" + value + "'}";
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Name name = (Name) o;
        return Objects.equals(value, name.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}

