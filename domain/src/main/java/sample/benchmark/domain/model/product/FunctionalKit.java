package sample.benchmark.domain.model.product;

import sample.benchmark.domain.model.basics.Component;
import sample.benchmark.domain.model.basics.Identifier;
import sample.benchmark.domain.model.basics.Name;
import sample.benchmark.domain.model.basics.Path;

public final class FunctionalKit {
    private final Component component;
    private final Variants variants;

    public static FunctionalKit kit(final String name) {
        return kit(Name.of(name));
    }

    public static FunctionalKit kit(final Name name) {
        return new FunctionalKit(Component.component(name), Variants.empty());
    }

    public FunctionalKit path(final Path path) {
        return new FunctionalKit(component.path(path), variants);
    }

    public FunctionalKit path(final Name... elements) {
        return path(Path.path(elements));
    }

    public FunctionalKit name(final Name name) {
        return new FunctionalKit(component.name(name), variants);
    }

    public FunctionalKit with(final Variant variant) {
        return new FunctionalKit(component, variants.add(variant.path(component.name(), variant.name())));
    }

    private FunctionalKit(final Component component, final Variants variants) {
        this.component = component;
        this.variants = variants;
    }

    public Identifier identifier() {
        return component.identifier();
    }

    public Path path() {
        return component.path();
    }

    public Name name() {
        return component.name();
    }

    public Variants variants() {
        return variants;
    }

    public int variantCount() {
        return variants.count();
    }

    public Variant variantByIndex(final int index) {
        return variants.findByIndex(index);
    }
}
