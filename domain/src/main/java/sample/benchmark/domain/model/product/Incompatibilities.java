package sample.benchmark.domain.model.product;

import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import io.vavr.collection.Set;
import sample.benchmark.domain.model.basics.Identifier;

public final class Incompatibilities {
    private final List<Incompatibility> incompatibilities;

    public static Incompatibilities empty() {
        return new Incompatibilities(List.empty());
    }

    public Incompatibilities add(final Incompatibility incompatibility) {
        return new Incompatibilities(incompatibilities.append(incompatibility));
    }

    private Incompatibilities(final List<Incompatibility> incompatibilities) {
        this.incompatibilities = incompatibilities;
    }

    public Set<Identifier> incompatibilitiesFor(final Identifier variant) {
        Set<Identifier> result = HashSet.empty();
        for (final Incompatibility incompatibility : incompatibilities) {
            if (incompatibility.isRelatedTo(variant)) {
                result = result.addAll(incompatibility.variants());
            }
        }
        return result.remove(variant);
    }

}
