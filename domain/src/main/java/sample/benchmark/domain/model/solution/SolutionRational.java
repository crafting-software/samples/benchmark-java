package sample.benchmark.domain.model.solution;

public interface SolutionRational {
    String describe();
}
