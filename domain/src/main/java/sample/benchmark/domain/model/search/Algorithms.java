package sample.benchmark.domain.model.search;

import sample.benchmark.domain.model.project.Project;

public final class Algorithms {
    public static Search bruteForce(final Project project) {
        return new BruteForce(project);
    }

    public static Search random(final Project project) {
        return new Random(project);
    }
}
