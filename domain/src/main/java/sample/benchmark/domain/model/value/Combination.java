package sample.benchmark.domain.model.value;

import io.vavr.collection.List;

import java.util.function.Function;

public interface Combination extends Function<List<Value>, Value> {
}
