package sample.benchmark.domain.model.solution;

import com.google.common.base.Joiner;
import io.vavr.collection.List;
import sample.benchmark.domain.model.basics.Path;
import sample.benchmark.domain.model.product.Variant;

public final class SolutionIncompatible implements SolutionRational {
    private final List<Variant> variants;

    SolutionIncompatible(final List<Variant> variants) {
        this.variants = variants;
    }

    public String describe() {
        return "Incompatibilities: " + Joiner.on(", ").join(variants.map(Variant::path).map(Path::render)) + "\n";
    }
}
