package sample.benchmark.domain.model.solution;

import com.google.common.base.Joiner;
import io.vavr.Tuple2;
import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import io.vavr.collection.Set;
import io.vavr.control.Option;
import sample.benchmark.domain.model.basics.Identifier;
import sample.benchmark.domain.model.metric.Metric;
import sample.benchmark.domain.model.product.Variant;
import sample.benchmark.domain.model.project.Criteria;
import sample.benchmark.domain.model.value.Value;

import static sample.benchmark.domain.model.solution.SolutionRationals.criteriaViolation;
import static sample.benchmark.domain.model.solution.SolutionRationals.variantsIncompatible;

public final class Solution {
    private final List<Variant> variants;
    private final Set<Identifier> incompatibilities;
    private final SolutionMetric metrics;
    private final List<Criteria> criterion;
    private final List<SolutionRational> rationals;

    public static Solution solution(final List<Variant> variants) {
        return new Solution(variants, HashSet.empty(), SolutionMetric.empty(), List.empty(), List.empty());
    }

    public Solution addIncompatibilities(final Set<Identifier> incompatibilities) {
        return new Solution(variants, incompatibilities.addAll(incompatibilities), metrics, criterion, List.empty());
    }

    public Solution addMetric(final Metric metric) {
        return new Solution(
                variants,
                incompatibilities,
                this.metrics.add(metric, metric.combineVariants(variants.map(Variant::identifier))),
                criterion, List.empty());
    }

    public Solution addCriteria(final Criteria criteria) {
        return new Solution(variants, incompatibilities, metrics, criterion.append(criteria), List.empty());
    }

    private Solution(final List<Variant> variants, final Set<Identifier> incompatibilities, final SolutionMetric metrics, final List<Criteria> criterion, final List<SolutionRational> rationals) {
        this.variants = variants;
        this.incompatibilities = incompatibilities;
        this.metrics = metrics;
        this.criterion = criterion;
        this.rationals = rationals;
    }

    public List<Variant> composition() {
        return variants;
    }

    public SolutionMetric metrics() {
        return metrics;
    }

    public List<Criteria> criterion() {
        return criterion;
    }

    public Solution update() {
        List<SolutionRational> rationals = List.empty();

        final List<Variant> incompatibleVariants = variants.filter(v -> incompatibilities.contains(v.identifier()));
        if (!incompatibleVariants.isEmpty()) {
            rationals = rationals.append(variantsIncompatible(incompatibleVariants));
        }

        for (final Criteria criteria : criterion) {
            final Option<Tuple2<Metric, Value>> tuple = metrics.findMetric(criteria);
            if (tuple.isDefined()) {
                if (Double.isNaN(tuple.get()._2.value())) {
                    continue;
                }
                if (!criteria.predicate().test(tuple.get()._2)) {
                    rationals = rationals.append(criteriaViolation(criteria, tuple.get()._1, tuple.get()._2));
                }
            }
        }

        return new Solution(variants, incompatibilities, metrics, criterion, rationals);
    }

    public boolean isValid() {
        return rationals.isEmpty();
    }

    public String describe() {
        final StringBuilder builder = new StringBuilder("# Solution ")
                .append(Joiner.on(", ").join(variants.map(variant -> variant.path().render())))
                .append("\n\n")
                .append("## Metrics\n")
                .append(metrics.describe())
                .append("\n");

        if (rationals.isEmpty()) {
            builder.append("## Status: ").append("*valid*\n");
        } else {
            builder.append("## Status: ").append("*rejected*\n").append("\n");
            for (final SolutionRational rational : rationals) {
                builder.append(rational.describe()).append("\n");
            }
        }
        return builder.append("\n\n").toString();
    }
}
