package sample.benchmark.domain.model.basics;

import java.util.Objects;

public final class Component {
    private final Identifier identifier;
    private final Path path;
    private final Name name;
    private final Description description;

    public static Component component(final String name) {
        return component(Name.of(name));
    }

    public static Component component(final Name name) {
        return new Component(Identifier.uuid(), Path.empty(), name, Description.empty());
    }

    public Component clone(final Identifier identifier) {
        return new Component(identifier, path, name, description);
    }

    public Component describe(final Description description) {
        return new Component(identifier, path, name, description);
    }

    public Component path(final Path path) {
        return new Component(identifier, path, name, description);
    }

    public Component path(final Name... elements) {
        return path(Path.path(elements));
    }

    public Component name(final Name name) {
        return new Component(identifier, path, name, description);
    }

    private Component(final Identifier identifier, final Path path, final Name name, final Description description) {
        this.identifier = identifier;
        this.path = path;
        this.name = name;
        this.description = description;
    }

    public Identifier identifier() {
        return identifier;
    }

    public Path path() {
        return path;
    }

    public Name name() {
        return name;
    }

    public Description description() {
        return description;
    }

    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Component component = (Component) o;
        return Objects.equals(identifier, component.identifier);
    }

    public int hashCode() {
        return Objects.hash(identifier);
    }
}
