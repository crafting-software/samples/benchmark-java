package sample.benchmark.domain.model.project;

import sample.benchmark.domain.model.basics.Component;
import sample.benchmark.domain.model.basics.Identifier;
import sample.benchmark.domain.model.basics.Name;
import sample.benchmark.math.Index;
import sample.benchmark.math.ConvexSpace;
import sample.benchmark.domain.model.metric.Metric;
import sample.benchmark.domain.model.metric.Metrics;
import sample.benchmark.domain.model.product.ProductLine;
import sample.benchmark.domain.model.solution.Solution;
import sample.benchmark.math.Space;

public final class Project {
    private final Component component;
    private final ProductLine product;
    private final Metrics metrics;
    private final Criterion criterion;

    public static Project project(final String name, final ProductLine product) {
        return project(Name.of(name), product);
    }

    public static Project project(final Name name, final ProductLine product) {
        return new Project(Component.component(name), product, Metrics.empty(), Criterion.empty());
    }

    public Project name(final Name name) {
        return new Project(component.name(name), product, metrics, criterion);
    }

    public Project add(final Metric metric) {
        return new Project(
                component,
                product,
                metrics.add(metric),
                criterion);
    }

    public Project add(final Criteria criteria) {
        return new Project(component, product, metrics, criterion.add(criteria));
    }

    private Project(final Component component, final ProductLine product, final Metrics metrics, final Criterion criterion) {
        this.component = component;
        this.product = product;
        this.metrics = metrics;
        this.criterion = criterion;
    }

    public Identifier identifier() {
        return component.identifier();
    }

    public Name name() {
        return component.name();
    }

    public ProductLine product() {
        return product;
    }

    public Space solutionSpace() {
        return product.solutionSpace();
    }

    public Solution evaluateFor(final Index index) {
        Solution solution = product.evaluateFor(index);
        for (final Criteria criteria : criterion.content()) {
            solution = solution.addCriteria(criteria);
        }
        for (final Metric metric : metrics.content()) {
            solution = solution.addMetric(metric);
        }
        return solution.update();
    }
}
