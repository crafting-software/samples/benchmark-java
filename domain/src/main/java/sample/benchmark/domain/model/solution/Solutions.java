package sample.benchmark.domain.model.solution;

import sample.benchmark.domain.model.project.Criteria;
import sample.benchmark.domain.model.project.CriteriaInstruction;
import sample.benchmark.math.pareto.ParetoComparators;

public final class Solutions {
    public static ParetoComparators.DoubleExtractor<Solution> extractOf() {
        return new ParetoComparators.DoubleExtractor<>() {
            public int size(final Solution solution) {
                return solution.criterion().size();
            }

            public double get(final Solution solution, final int i) {
                final Criteria criteria = solution.criterion().get(i);
                final double value = solution.metrics().findMetric(criteria).get()._2.value();
                if (criteria.instruction() == CriteriaInstruction.MAXIMIZE) {
                    return -value;
                } else {
                    return value;
                }
            }
        };
    }
}
