package sample.benchmark.domain.model.product;

import io.vavr.collection.Iterator;
import io.vavr.collection.LinkedHashMap;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import io.vavr.control.Option;
import sample.benchmark.domain.model.basics.Identifier;
import sample.benchmark.domain.model.solution.Solution;
import sample.benchmark.math.ConvexSpace;
import sample.benchmark.math.Index;
import sample.benchmark.math.Space;

import static sample.benchmark.domain.model.solution.Solution.solution;

public final class FunctionalKits {
    private final Map<Identifier, FunctionalKit> kits;

    public static FunctionalKits empty() {
        return new FunctionalKits(LinkedHashMap.empty());
    }

    public FunctionalKits add(final FunctionalKit kit) {
        return new FunctionalKits(kits.put(kit.identifier(), kit));
    }

    private FunctionalKits(final Map<Identifier, FunctionalKit> kits) {
        this.kits = kits;
    }

    public List<FunctionalKit> content() {
        return kits.values().toList();
    }

    public FunctionalKit findByIndex(final int index) {
        return content().get(index);
    }

    public Iterator<FunctionalKit> iterate() {
        return kits.values().iterator();
    }

    public Space solutionSpace() {
        return ConvexSpace.of(content().map(FunctionalKit::variantCount));
    }

    public Solution evaluateFor(final Index index) {
        return solution(index.map((variant, kit) -> findByIndex(kit).variantByIndex(variant)));
    }

    public String toString() {
        return "FunctionalKits{" + kits + '}';
    }

    public Option<FunctionalKit> kit(final Identifier identifier) {
        return kits.get(identifier);
    }
}
