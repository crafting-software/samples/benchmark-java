package sample.benchmark.domain.model.value;

public final class Values {
    public static Value quantity(final double value) {
        return new Quantity(value);
    }

    public static Value grade(final double value) {
        return new Grade(value);
    }

}
