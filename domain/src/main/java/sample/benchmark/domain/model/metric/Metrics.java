package sample.benchmark.domain.model.metric;

import io.vavr.collection.HashMap;
import io.vavr.collection.Iterator;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import io.vavr.control.Option;
import sample.benchmark.domain.model.basics.Identifier;

public final class Metrics {
    private final Map<Identifier, Metric> metrics;

    public static Metrics empty() {
        return new Metrics(HashMap.empty());
    }

    public Metrics add(final Metric metric) {
        return new Metrics(metrics.put(metric.identifier(), metric));
    }

    private Metrics(final Map<Identifier, Metric> metrics) {
        this.metrics = metrics;
    }

    public List<Metric> content() {
        return metrics.values().toList();
    }

    public Option<Metric> findByIdentifier(final Identifier identifier) {
        return metrics.get(identifier);
    }

    public Iterator<Metric> iterate() {
        return metrics.values().iterator();
    }
}
