package sample.benchmark.domain.usecase;

import com.google.common.base.Joiner;
import com.google.common.collect.Maps;
import io.vavr.control.Option;
import org.junit.jupiter.api.Test;
import sample.benchmark.domain.model.basics.Identifier;
import sample.benchmark.domain.model.basics.Name;
import sample.benchmark.domain.model.product.ProductLine;

import java.util.Map;

import static io.vavr.collection.List.ofAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

class CreateProductUnitTest {
    @Test
    void should_be_able_to_create_new_product() {
        given_empty_repository()
                .when_I_create_product_named("Car")
                .then_repository_should_contains_products("Car");
    }

    interface Scenario {
        Scenario when_I_create_product_named(String name);

        void then_repository_should_contains_products(String name);
    }

    private Scenario given_empty_repository() {
        final Map<Identifier, ProductLine> content = Maps.newHashMap();
        final ProductLineRepository repository = new ProductLineRepository() {
            public Option<ProductLine> findById(final Identifier product) {
                return Option.of(content.get(product));
            }

            public ProductLine save(final ProductLine product) {
                return content.put(product.identifier(), product);
            }
        };
        return new Scenario() {
            public Scenario when_I_create_product_named(final String name) {
                new CreateProduct(repository).process(Name.of(name));
                return this;
            }

            public void then_repository_should_contains_products(final String names) {
                assertEquals(
                        names,
                        Joiner.on(", ").join(ofAll(content.values()).map(ProductLine::name).map(Name::value).sorted()));
            }
        };
    }

}
