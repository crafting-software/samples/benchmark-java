package sample.benchmark.math.pareto;

import com.google.common.base.Joiner;
import org.junit.jupiter.api.Test;
import sample.benchmark.math.Index;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static sample.benchmark.math.Index.index;

class ParetoFrontTest {
    @Test
    void an_empty_front_should_be_empty() {
        assertEquals("Front{}", frontOf());
    }

    @Test
    void a_front_with_only_one_element() {
        assertEquals("Front{(2, 2)}", frontOf(index(2, 2)));
    }

    @Test
    void a_front_with_two_elements_first_dominate() {
        assertEquals("Front{(3, 3)}", frontOf(index(3, 3), index(4, 4)));
    }

    @Test
    void a_front_with_two_elements_second_dominate() {
        assertEquals("Front{(3, 3)}", frontOf(index(4, 4), index(3, 3)));
    }

    @Test
    void a_front_with_two_equals_elements() {
        assertEquals("Front{(2, 2)}", frontOf(index(2, 2), index(2, 2)));
    }

    @Test
    void a_front_with_two_elements_none_dominate() {
        assertEquals("Front{(3, 2), (2, 3)}", frontOf(index(3, 2), index(2, 3)));
    }

    @Test
    void a_front_with_some_elements() {
        assertEquals(
                "Front{(3, 2), (2, 3), (4, 1)}",
                frontOf(index(3, 2), index(2, 3), index(5, 5), index(5, 6), index(6, 6), index(4, 1)));
    }

    @Test
    void a_front_with_more_dimensions_in_solutions() {
        assertEquals(
                "Front{(3, 2, 4), (2, 3, 0)}",
                frontOf(index(3, 2, 4), index(2, 3, 0), index(4, 5, 7)));
    }


    private static String frontOf(final Index... solutions) {
        ParetoFront<Index> front = ParetoFront.empty(ParetoComparators.comparatorInDouble(valuesOf()));
        for (final Index solution : solutions) {
            front = front.add(solution);
        }
        return render(front);
    }

    private static String render(final ParetoFront<Index> front) {
        return "Front{" + Joiner.on(", ").join(front.solutions()) + "}";
    }

    private static ParetoComparators.DoubleExtractor<Index> valuesOf() {
        return new ParetoComparators.DoubleExtractor<>() {
            public int size(final Index index) {
                return index.size();
            }

            public double get(final Index index, final int i) {
                return index.get(i);
            }
        };
    }
}