package sample.benchmark.math;

import org.junit.jupiter.api.Test;

import static com.google.common.collect.Iterators.size;
import static java.util.stream.Collectors.joining;
import static org.junit.jupiter.api.Assertions.assertEquals;


class IndexIteratorUnitTest {
    @Test
    void a_index_iterator_should_iterate_on_single_dimension() {
        assertEquals("(0), (1), (2)", render(IndexIterator.iterate(3)));
    }


    @Test
    void a_index_iterator_should_iterate_on_two_dimensions() {
        assertEquals(
                "(0, 0), (1, 0), (2, 0), (0, 1), (1, 1), (2, 1)",
                render(IndexIterator.iterate(3, 2)));
    }

    @Test
    void a_index_iterator_should_make_cartesian_product() {
        assertEquals(4 * 2 * 3, size(IndexIterator.iterate(4, 2, 3)));
    }

    private static String render(final IndexIterator iterate) {
        return iterate.map(Index::toString).collect(joining(", "));
    }
}
