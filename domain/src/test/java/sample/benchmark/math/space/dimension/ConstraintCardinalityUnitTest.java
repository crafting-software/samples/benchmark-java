package sample.benchmark.math.space.dimension;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static sample.benchmark.math.space.dimension.Constraints.*;

class ConstraintCardinalityUnitTest {

    @Test
    void cardinality_of_empty_constraint() {
        assertEquals(0, none().cardinality());
    }

    @Test
    void cardinality_of_range() {
        assertEquals(10, range(10).cardinality());
        assertEquals(10, range(10, 20).cardinality());
    }

    @Test
    void cardinality_of_element() {
        assertEquals(1, eq(3).cardinality());
    }
}