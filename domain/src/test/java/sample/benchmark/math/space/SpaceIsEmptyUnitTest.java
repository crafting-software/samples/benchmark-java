package sample.benchmark.math.space;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static sample.benchmark.math.space.Spaces.empty;
import static sample.benchmark.math.space.Spaces.space;
import static sample.benchmark.math.space.dimension.Constraints.none;
import static sample.benchmark.math.space.dimension.Constraints.range;
import static sample.benchmark.math.space.dimension.Dimension.dimension;

class SpaceIsEmptyUnitTest {

    @Test
    void an_empty_space_is_empty() {
        assertTrue(empty().isEmpty());
    }

    @Test
    void a_space_with_empty_dimension_is_empty() {
        assertTrue(space(dimension("t", none())).isEmpty());
    }

    @Test
    void a_space_with_non_empty_dimension_is_not_empty() {
        assertFalse(space(dimension("t", range(10))).isEmpty());
    }

}