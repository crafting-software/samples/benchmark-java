package sample.benchmark.math.space;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static sample.benchmark.math.space.Coordinate.c;
import static sample.benchmark.math.space.Spaces.*;
import static sample.benchmark.math.space.dimension.Constraints.eq;
import static sample.benchmark.math.space.dimension.Constraints.range;
import static sample.benchmark.math.space.dimension.Dimension.dimension;

class SpaceContainsUnitTest {

    @Test
    void an_empty_space_contains_nothing() {
        assertFalse(empty().contains(Coordinates.of(c("x", 1))));
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 1, 2})
    void space_with_range_dimension_should_contains_all_inside_coordinates(final int index) {
        assertTrue(space(dimension("x", range(3))).contains(Coordinates.of(c("x", index))));
    }

    @ParameterizedTest
    @ValueSource(ints = {-1, 3, 4, 10})
    void space_with_range_dimension_should_not_contains_outside_coordinates(final int index) {
        assertFalse(space(dimension("x", range(3))).contains(Coordinates.of(c("x", index))));
    }

    @ParameterizedTest
    @ValueSource(ints = {3, 5, 7})
    void space_with_specific_dimension_should_contains_all_inside_coordinates(final int index) {
        assertTrue(space(dimension("x", range(9))).contains(Coordinates.of(c("x", index))));
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 1, 5, 6, 8, 9})
    void space_with_specific_dimension_should_not_contains_outside_coordinates(final int index) {
        assertFalse(space(dimension("x", range(3, 5))).contains(Coordinates.of(c("x", index))));
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 1, 2, 9, 10})
    void union_space_should_contains_inside_coordinates(final int index) {
        assertTrue(union(space(dimension("x", range(0, 3))), space(dimension("x", range(9, 11)))).contains(Coordinates.of(c("x", index))));
    }

    @ParameterizedTest
    @ValueSource(ints = {-1, 3, 4, 5, 6, 7, 8, 11, 12, 13})
    void union_space_should_not_contains_outside_coordinates(final int index) {
        assertFalse(union(space(dimension("x", range(0, 3))), space(dimension("x", range(9, 11)))).contains(Coordinates.of(c("x", index))));
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 1, 2, 3, 4, 5, 6, 8, 9})
    void difference_space_should_contains_inside_coordinates(final int index) {
        assertTrue(difference(space(dimension("x", range(10))), space(dimension("x", eq(7)))).contains(Coordinates.of(c("x", index))));
    }

    @ParameterizedTest
    @ValueSource(ints = {7, 10, 11, 12})
    void difference_space_should_not_contains_outside_coordinates(final int index) {
        assertFalse(difference(space(dimension("x", range(10))), space(dimension("x", eq(7)))).contains(Coordinates.of(c("x", index))));
    }
}