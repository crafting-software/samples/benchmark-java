package sample.benchmark.math.space;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static sample.benchmark.math.space.Spaces.*;
import static sample.benchmark.math.space.dimension.Constraints.eq;
import static sample.benchmark.math.space.dimension.Constraints.range;
import static sample.benchmark.math.space.dimension.Dimension.dimension;

class SpaceIntersectionUnitTest {
    private final Space EMPTY = empty();


    @Test
    void intersection_with_empty_space_is_empty() {
        assertEquals("{}", render(intersection(
                space(dimension("t", range(10))),
                EMPTY)));
    }

    @Test
    void intersection_with_space_without_common_dimension_is_empty() {
        assertEquals("{}", render(intersection(
                space(dimension("t", range(10))),
                space(dimension("u", range(5))))));
    }

    @Test
    void intersection_with_space_with_common_dimension_but_no_intersection_is_empty() {
        assertEquals("{}", render(intersection(
                space(dimension("t", eq(9))),
                space(dimension("t", eq(4))))));

        assertEquals("{}", render(intersection(
                space(dimension("t", range(10))),
                space(dimension("t", range(20, 30))))));
    }


    @Test
    void general_case_of_intersections() {
        assertEquals("{t: {4}}", render(intersection(
                space(dimension("t", range(10))),
                space(dimension("t", eq(4))))));

        assertEquals("{t: {4}}", render(intersection(
                space(dimension("t", eq(4))),
                space(dimension("t", eq(4))))));

        assertEquals("{u: {5}, v: {5}}", render(intersection(
                space(dimension("u", range(10)), dimension("v", eq(5))),
                space(dimension("u", eq(5)), dimension("v", range(20))))));
    }


    @Test
    void general_case_of_constraints_intersection() {
        assertEquals("{}", render(intersection(
                space(dimension("t", range(10))),
                space(dimension("u", range(5))))));

        assertEquals("{t: {4}}", render(intersection(
                space(dimension("t", range(10))),
                space(dimension("t", eq(4))))));

        assertEquals("{}", render(intersection(
                space(dimension("t", eq(9))),
                space(dimension("t", eq(4))))));

        assertEquals("{t: {4}}", render(intersection(
                space(dimension("t", eq(4))),
                space(dimension("t", eq(4))))));

        assertEquals("{u: {5}, v: {5}}", render(intersection(
                space(dimension("u", range(10)), dimension("v", eq(5))),
                space(dimension("u", eq(5)), dimension("v", range(20))))));
    }

    @Test
    void intersection_of_union() {
        final Space full = intersection(
                space(
                        dimension("t", range(3)),
                        dimension("u", range(3)),
                        dimension("v", range(3))),
                union(
                        space(dimension("t", eq(1)), dimension("u", eq(1)), dimension("v", range(3))),
                        union(space(dimension("t", eq(1)), dimension("u", eq(2)), dimension("v", range(3))),
                                union(space(dimension("t", eq(1)), dimension("u", range(3)), dimension("v", eq(1))),
                                        space(dimension("t", eq(1)), dimension("u", range(3)), dimension("v", eq(2)))))));
        assertEquals("U ({t: {1}, u: {1}, v: {0...3}}, U ({t: {1}, u: {2}, v: {0...3}}, U ({t: {1}, u: {0...3}, v: {1}}, {t: {1}, u: {0...3}, v: {2}})))", render(full));
    }

    @Test
    void intersection_of_different_dimension_should_be_empty() {
        assertEquals("{}", render(intersection(
                space(dimension("u", eq(1)), dimension("v", eq(1))),
                space(dimension("u", eq(1)), dimension("w", eq(1))))));
    }

    private static String render(final Space space) {
        return space.render();
    }
}