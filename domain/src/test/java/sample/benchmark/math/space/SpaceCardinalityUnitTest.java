package sample.benchmark.math.space;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static sample.benchmark.math.space.Spaces.*;
import static sample.benchmark.math.space.Spaces.union;
import static sample.benchmark.math.space.dimension.Constraints.*;
import static sample.benchmark.math.space.dimension.Dimension.dimension;

class SpaceCardinalityUnitTest {

    @Test
    void cardinality_of_empty_space_is_zero() {
        assertEquals(0, empty().cardinality());
    }

    @Test
    void a_non_empty_space_has_product_of_all_dimensions_cardinality() {
        assertEquals(200, space(dimension("t", range(10)), dimension("u", range(20))).cardinality());
        assertEquals(10, space(dimension("t", range(10)), dimension("u", eq(4))).cardinality());
    }

    @Test
    void cardinality_of_union_of_disjoint_spaces() {
        final Space first = space(dimension("u", range(10)), dimension("v", eq(4)), dimension("w", eq(5)));
        final Space second = space(dimension("u", eq(4)), dimension("v", eq(5)), dimension("w", range(10)));

        assertEquals(20, union(first, second).cardinality());
    }

    @Test
    void cardinality_of_union_of_intersection_constraints() {
        final Space first = space(dimension("u", range(10)), dimension("v", eq(1)), dimension("w", eq(3)), dimension("x", range(10)));
        final Space second = space(dimension("u", eq(4)), dimension("v", eq(1)), dimension("w", range(10)), dimension("x", range(10)));

        assertEquals(190, union(first, second).cardinality());
    }

    @Test
    void case_with_bug() {
        final Space remove = union(
                space(dimension("u", eq(1)), dimension("v", eq(1)), dimension("w", range(3))),
                union(space(dimension("u", eq(1)), dimension("v", eq(2)), dimension("w", range(3))),
                        union(space(dimension("u", eq(1)), dimension("v", range(3)), dimension("w", eq(1))),
                                space(dimension("u", eq(1)), dimension("v", range(3)), dimension("w", eq(2))))));
        assertEquals(8, remove.cardinality());
    }

    @Test
    void case_with_bug_() {
        final Space remove = union(
                space(dimension("u", eq(0)), dimension("v", eq(0)), dimension("w", range(2))),
                union(space(dimension("u", eq(0)), dimension("v", range(2)), dimension("w", eq(0))),
                        union(space(dimension("u", range(2)), dimension("v", eq(0)), dimension("w", eq(0))),
                                union(space(dimension("u", eq(1)), dimension("v", eq(1)), dimension("w", range(2))),
                                        union(space(dimension("u", eq(1)), dimension("v", range(2)), dimension("w", eq(1))),
                                                space(dimension("u", range(2)), dimension("v", eq(1)), dimension("w", eq(1))))))));
        assertEquals(8, remove.cardinality());
    }

    @Test
    void cardinality_of_difference_of_overlapping_spaces_should() {
        assertEquals(3, difference(
                space(dimension("x", range(3, 7))),
                space(dimension("x", eq(5)))).cardinality());
    }

    @Test
    void cardinality_of_difference_of_non_overlapping_spaces_should() {
        assertEquals(3, difference(
                space(dimension("x", range(3))),
                space(dimension("x", eq(9)))).cardinality());
    }
}