package sample.benchmark.math.space.dimension;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static sample.benchmark.math.space.dimension.Constraints.none;
import static sample.benchmark.math.space.dimension.Constraints.range;

class ConstraintIterationUnitTest {

    @Test
    void empty_constraint_should_have_no_first_value() {
        assertTrue(none().first().isEmpty());
    }

    @ParameterizedTest
    @ValueSource(ints = {-1, 0, 3, 5, 2000})
    void empty_constraint_should_have_no_next_value(final int index) {
        assertTrue(none().next(index).isEmpty());
    }

    @Test
    void range_constraint_should_have_first_value() {
        assertEquals(0, range(0, 10).first().get());
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 1, 2, 3, 8})
    void range_constraint_should_have_next_value_when_index_is_inside_range(final int index) {
        assertEquals(index + 1, range(0, 10).next(index).get());
    }

    @ParameterizedTest
    @ValueSource(ints = {-1, 9, 10, 2000})
    void range_constraint_should_have_no_next_value_otherwise(final int index) {
        assertTrue(range(0, 10).next(index).isEmpty());
    }
}