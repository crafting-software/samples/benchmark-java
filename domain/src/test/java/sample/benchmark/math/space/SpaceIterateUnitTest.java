package sample.benchmark.math.space;

import com.google.common.base.Joiner;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static sample.benchmark.math.space.Spaces.*;
import static sample.benchmark.math.space.dimension.Constraints.contain;
import static sample.benchmark.math.space.dimension.Constraints.eq;
import static sample.benchmark.math.space.dimension.Constraints.range;
import static sample.benchmark.math.space.dimension.Dimension.dimension;

class SpaceIterateUnitTest {

    @Test
    void an_empty_space_is_empty() {
        assertEquals("{}", iterate(empty()));
    }

    @Test
    void an_space_should_iterate_all_elements() {
        assertEquals("{(x=0), (x=1), (x=2)}", iterate(space(dimension("x", range(3)))));
        assertEquals("{(x=0, y=0), (x=1, y=0), (x=2, y=0), (x=0, y=1), (x=1, y=1), (x=2, y=1)}", iterate(space(dimension("x", range(3)), dimension("y", range(2)))));
    }

    @Test
    void an_space_should_iterate_all_elements_() {
        assertEquals("{(x=0, y=2), (x=1, y=2)}", iterate(space(dimension("x", range(2)), dimension("y", eq(2)))));
    }

    @Test
    void an_union_of_space_should_iterate_all_elements_() {
        assertEquals("{(x=0, y=2), (x=1, y=2), (x=4, y=0), (x=5, y=0), (x=4, y=1), (x=5, y=1)}", iterate(
                union(
                        space(dimension("x", range(2)), dimension("y", eq(2))),
                        space(dimension("x", range(4, 6)), dimension("y", range(2))))));
    }

    @Test
    void an_difference_of_space_should_iterate_all_elements_() {
        assertEquals("{(x=0, y=0), (x=1, y=0), (x=2, y=0), (x=0, y=2), (x=1, y=2), (x=2, y=2)}", iterate(
                difference(
                        space(dimension("x", range(3)), dimension("y", range(3))),
                        space(dimension("x", range(3)), dimension("y", eq(1))))));
    }

    private static String iterate(final Space space) {
        return "{" + Joiner.on(", ").join(space.iterate().map(Coordinates::render).toList()) + "}";
    }
}