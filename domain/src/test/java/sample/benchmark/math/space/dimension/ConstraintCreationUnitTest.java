package sample.benchmark.math.space.dimension;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static sample.benchmark.math.space.dimension.Constraints.*;

class ConstraintCreationUnitTest {

    @Test
    void creation_of_empty_constraint() {
        assertEquals("{}", render(none()));
    }

    @Test
    void creation_of_range_constraint() {
        assertEquals("{0...10}", render(range(10)));
        assertEquals("{7...32}", render(range(7, 32)));
    }

    @Test
    void creation_of_eq_constraint() {
        assertEquals("{3}", render(eq(3)));
    }

    private static String render(final Constraint constraint) {
        return constraint.render();
    }

}