package sample.benchmark.math.space.dimension;

import io.vavr.collection.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.*;
import static sample.benchmark.math.space.dimension.Constraints.*;

class ConstraintIsEmptyUnitTest {

    @Test
    void is_empty_of_empty_constraint() {
        assertTrue(none().isEmpty());
    }

    @Test
    void empty_constraint_should_have_no_elements() {
        assertEquals(0, none().values().length());
    }

    private static List<Constraint> some() {
        return List.of(
                range(10),
                range(7, 54),
                eq(4)
        );
    }

    @ParameterizedTest
    @MethodSource("some")
    void intersection_with_empty_constraint_is_empty(final Constraint constraint) {
        assertFalse(constraint.isEmpty());
    }
}