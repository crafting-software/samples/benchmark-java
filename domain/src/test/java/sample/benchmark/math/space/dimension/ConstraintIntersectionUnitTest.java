package sample.benchmark.math.space.dimension;

import io.vavr.collection.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static sample.benchmark.math.space.dimension.Constraints.*;

class ConstraintIntersectionUnitTest {

    private static List<Constraint> some() {
        return List.of(
                none(),
                range(10),
                range(7, 54),
                eq(4)
        );
    }

    @ParameterizedTest
    @MethodSource("some")
    void intersection_with_empty_constraint_is_empty(final Constraint constraint) {
        assertEquals("{}", render(intersection(none(), constraint)));
        assertEquals("{}", render(intersection(constraint, none())));
    }

    @ParameterizedTest
    @MethodSource("some")
    void intersection_with_two_same_constraints_is_identity(final Constraint constraint) {
        assertEquals(render(constraint), render(intersection(constraint, constraint)));
    }

    @Test
    void intersection_two_disjoint_ranges_is_empty() {
        assertEquals("{}", render(intersection(range(3, 7), range(13, 18))));
    }

    @Test
    void intersection_two_disjoint_elements_is_empty() {
        assertEquals("{}", render(intersection(eq(3), eq(2))));
    }

    @Test
    void intersection_two_overlapping_ranges_is_a_range() {
        assertEquals("{7...10}", render(intersection(range(3, 10), range(7, 18))));
    }

    @Test
    void intersection_two_same_elements() {
        assertEquals("{3}", render(intersection(eq(3), eq(3))));
    }

    private static String render(final Constraint constraint) {
        return constraint.render();
    }
}