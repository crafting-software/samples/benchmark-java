package sample.benchmark.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static sample.benchmark.math.Index.index;


class IndexUnitTest {
    @Test
    void an_index_should_be_readable() {
        assertEquals("(2, 3)", index(2, 3).toString());
    }
}
