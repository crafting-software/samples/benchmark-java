package sample.benchmark.math.constraints;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static sample.benchmark.math.constraints.Dimensions.*;

class DimensionsUnitTest {
    @Test
    void an_zero_element_dimension_has_an_empty_intersection() {
        assertEquals("{}", render(zero().intersection(zero())));
        assertEquals("{}", render(zero().intersection(one(2))));
        assertEquals("{}", render(zero().intersection(many(10))));
    }

    @Test
    void a_many_elements_dimension_has_fixed_intersection_with_a_one_element_dimension() {
        assertEquals("{4}", render(many(10).intersection(one(4))));
    }

    @Test
    void a_many_elements_dimension_has_ranged_intersection_with_a_many_elements_dimension() {
        assertEquals("{0..10}", render(many(10).intersection(many(10))));
    }

    @Test
    void a_many_elements_dimension_has_empty_intersection_with_a_zero_element_dimension() {
        assertEquals("{}", render(many(10).intersection(zero())));
    }

    @Test
    void a_one_element_dimension_has_fixed_intersection_with_a_one_element_dimension_when_same_value() {
        assertEquals("{4}", render(one(4).intersection(one(4))));
    }

    @Test
    void a_one_element_dimension_has_empty_intersection_with_a_one_element_dimension_when_different_value() {
        assertEquals("{}", render(one(4).intersection(one(5))));
    }

    @Test
    void a_one_element_dimension_is_the_intersection_with_a_many_elements_dimension() {
        assertEquals("{4}", render(one(4).intersection(many(10))));
    }

    @Test
    void a_one_element_dimension_has_empty_intersection_with_a_zero_element_dimension() {
        assertEquals("{}", render(one(4).intersection(zero())));
    }

    @Test
    void a_zero_element_dimension_has_cardinality_of_0() {
        assertEquals(0, zero().cardinality());
    }

    @Test
    void a_one_element_dimension_has_cardinality_of_1() {
        assertEquals(1, one(5).cardinality());
        assertEquals(1, one(2).cardinality());
        assertEquals(1, one(200).cardinality());
    }

    @Test
    void a_many_elements_dimension_has_cardinality_of_its_size() {
        assertEquals(5, many(5).cardinality());
        assertEquals(2, many(2).cardinality());
        assertEquals(200, many(200).cardinality());
    }

    private static String render(final Dimension dimension) {
        return dimension.render();
    }

}