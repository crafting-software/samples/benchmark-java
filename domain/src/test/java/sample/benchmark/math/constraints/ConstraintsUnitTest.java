package sample.benchmark.math.constraints;

import io.vavr.collection.List;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static sample.benchmark.math.constraints.Constraints.dimension;
import static sample.benchmark.math.constraints.Constraints.empty;
import static sample.benchmark.math.constraints.Dimensions.many;
import static sample.benchmark.math.constraints.Dimensions.one;

class ConstraintsUnitTest {
    @Test
    void an_empty_constraint_has_always_empty_intersection() {
        assertEquals("{}", render(empty().intersection(empty())));
        assertEquals("{}", render(empty().intersection(dimension(one(4)))));
        assertEquals("{}", render(dimension(one(4)).intersection(empty())));
    }

    @Test
    void general_case_of_constraints_intersection() {
        assertEquals("{{4}}", render(dimension(many(10)).intersection(dimension(one(4)))));
        assertEquals("{}", render(dimension(one(10)).intersection(dimension(one(4)))));
        assertEquals("{{4}}", render(dimension(one(4)).intersection(dimension(one(4)))));
        assertEquals("{{4}, {5}}", render(dimension(many(10), one(5)).intersection(dimension(one(4), many(20)))));
        assertEquals("{{4}, {0..20}}", render(dimension(many(10), many(20)).intersection(dimension(one(4), many(20)))));
    }

    @Test
    void cardinality_of_constraints() {
        assertEquals(0, empty().cardinality());
        assertEquals(1, dimension(one(4)).cardinality());
        assertEquals(10, dimension(one(4), many(10)).cardinality());
        assertEquals(40, dimension(many(4), many(10)).cardinality());
    }

    @Test
    void cardinality_of_union_of_disjoint_constraints() {
        final Constraint first = dimension(many(10), one(4), one(5));
        final Constraint second = dimension(one(4), one(5), many(10));

        assertEquals(20, cardinalityOfUnion(first, second));
    }

    @Test
    void cardinality_of_union_of_intersection_constraints() {
        final Constraint first = dimension(many(10), one(1), one(3), many(10));
        final Constraint second = dimension(one(4), one(1), many(10), many(10));

        assertEquals(190, cardinalityOfUnion(first, second));
    }


    private static int cardinalityOfUnion(final Constraint... constraints) {
        return Constraints.cardinalityOfUnion(List.of(constraints));
    }

    private static String render(final Constraint constraint) {
        return constraint.render();
    }

}