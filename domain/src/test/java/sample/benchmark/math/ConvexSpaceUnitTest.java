package sample.benchmark.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static sample.benchmark.math.Index.index;


class ConvexSpaceUnitTest {
    @Test
    void a_space_should_compute_total_of_combinations() {
        assertEquals(6L, ConvexSpace.of(2, 3).combinations());
        assertEquals(2176782336L, ConvexSpace.of(6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6).combinations());
    }

    @Test
    void a_space_should_give_zero_index() {
        assertEquals("(0, 0)", ConvexSpace.of(2, 3).first().toString());
    }

    @Test
    void a_space_should_compute_next_index() {
        assertEquals("Some((1, 0))", ConvexSpace.of(2, 2).next(index(0, 0)).toString());
        assertEquals("Some((0, 1))", ConvexSpace.of(2, 2).next(index(1, 0)).toString());
        assertEquals("None", ConvexSpace.of(2, 2).next(index(1, 1)).toString());
    }

    @Test
    void a_space_should_determine_if_contains_index() {
        final ConvexSpace space = ConvexSpace.of(4, 4);
        assertTrue(space.contains(index(0, 3)));
        assertFalse(space.contains(index(4, 0)));
        assertFalse(space.contains(index(0, 4)));
        assertFalse(space.contains(index(0)));
        assertFalse(space.contains(index(0, 0, 0)));
    }

    @Test
    void a_space_should_give_random_index() {
        final ConvexSpace space = ConvexSpace.of(4, 4);
        assertTrue(space.contains(space.random()));
    }
}
